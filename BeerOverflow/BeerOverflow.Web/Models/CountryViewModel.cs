﻿namespace BeerOverflow.Web.Models
{
    public class CountryViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
