﻿namespace BeerOverflow.Web.Models
{
    public class StyleViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
