﻿namespace BeerOverflow.Web.Models
{
    public class RatingViewModel
    {
        public int Value { get; set; }
    }
}
