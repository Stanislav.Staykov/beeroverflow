﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class BeerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
        public string Brewery { get; set; }
        public string Description { get; set; }
        public ICollection<ReviewViewModel> Reviews { get; set; }
        public string Style { get; set; }
        public string ABV { get; set; }
    }
}
