﻿namespace BeerOverflow.Services.DTOs
{
    public class WishListViewModel
    {
        public int UserID { get; set; }
        public int BeerID { get; set; }
    }
}
