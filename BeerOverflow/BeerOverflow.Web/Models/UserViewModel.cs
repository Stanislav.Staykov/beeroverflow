﻿namespace BeerOverflow.Web.Models
{
    public class UserViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public bool IsBanned { get; set; }
        public string BanDescription { get; set; }
    }
}
