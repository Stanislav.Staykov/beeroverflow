﻿using System.Collections.Generic;

namespace BeerOverflow.Web.Models
{
    public class AllBeersViewModel
    {
        public List<BeerViewModel> AllBeers { get; set; }
    }
}
