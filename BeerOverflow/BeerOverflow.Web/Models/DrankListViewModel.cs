﻿namespace BeerOverflow.Services.DTOs
{
    public class DrankListViewModel
    {
        public int UserID { get; set; }
        public int BeerID { get; set; }
    }
}
