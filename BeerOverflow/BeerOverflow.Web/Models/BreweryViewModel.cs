﻿namespace BeerOverflow.Web.Models
{
    public class BreweryViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
    }
}
