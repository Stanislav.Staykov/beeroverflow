﻿using System;

namespace BeerOverflow.Services.DTOs
{
    public class ReviewViewModel
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int Likes { get; set; }
        public string Body { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
    }
}
