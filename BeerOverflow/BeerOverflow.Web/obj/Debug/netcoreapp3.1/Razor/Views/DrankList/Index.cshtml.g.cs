#pragma checksum "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2ad52075edaa3d3a2edfbf3ebbd2dbc74df22f96"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_DrankList_Index), @"mvc.1.0.view", @"/Views/DrankList/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\_ViewImports.cshtml"
using BeerOverflow.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\_ViewImports.cshtml"
using BeerOverflow.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2ad52075edaa3d3a2edfbf3ebbd2dbc74df22f96", @"/Views/DrankList/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9a357c2fbb3457365a0266be331b3e50a5bcc811", @"/Views/_ViewImports.cshtml")]
    public class Views_DrankList_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<BeerOverflow.Web.Models.BeerViewModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Remove", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "DrankList", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
  
    ViewData["Title"] = "Drank List";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>My Drank List Of Beers</h1>\r\n\r\n<div class=\"row\">\r\n");
#nullable restore
#line 10 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
     foreach (var item in Model)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"col-lg-4 col-sm-6\">\r\n            <div class=\"product text-center\">\r\n                <div class=\"mb-3 position-relative\">\r\n                    <div class=\"badge text-white badge-\"></div><a class=\"d-block\"");
            BeginWriteAttribute("href", " href=\"", 427, "\"", 456, 2);
            WriteAttributeValue("", 434, "Beers/Details/", 434, 14, true);
#nullable restore
#line 15 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
WriteAttributeValue("", 448, item.Id, 448, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral("><img class=\"img-fluid w-100\"");
            BeginWriteAttribute("src", " src=\"", 486, "\"", 538, 1);
#nullable restore
#line 15 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
WriteAttributeValue("", 492, Url.Content($"~/img/beers/beer{item.Id}.jpg"), 492, 46, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("onerror", " onerror=\'", 539, "\'", 604, 3);
            WriteAttributeValue("", 549, "this.src=\"", 549, 10, true);
#nullable restore
#line 15 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
WriteAttributeValue("", 559, Url.Content($"~/img/beers/defaultbeer.jpg"), 559, 44, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 603, "\"", 603, 1, true);
            EndWriteAttribute();
            WriteLiteral("></a>\r\n                    <div class=\"product-overlay\">\r\n                        <ul class=\"mb-0 list-inline\">\r\n                            <li class=\"list-inline-item mr-0\">\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2ad52075edaa3d3a2edfbf3ebbd2dbc74df22f966943", async() => {
                WriteLiteral("\r\n                                    <input type=\"hidden\" name=\"beerId\"");
                BeginWriteAttribute("value", " value=\"", 953, "\"", 969, 1);
#nullable restore
#line 20 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
WriteAttributeValue("", 961, item.Id, 961, 8, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n                                    <button class=\"btn btn-sm btn-dark\" type=\"submit\">\r\n                                        <i class=\"fas fa-trash\"></i>\r\n                                    </button>\r\n                                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                            </li>\r\n                            <li class=\"list-inline-item m-0 p-0\"><a class=\"btn btn-sm btn-dark\"");
            BeginWriteAttribute("href", " href=\"", 1351, "\"", 1380, 2);
            WriteAttributeValue("", 1358, "Beers/Details/", 1358, 14, true);
#nullable restore
#line 26 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
WriteAttributeValue("", 1372, item.Id, 1372, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Details</a></li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n                <h6> <a class=\"reset-anchor\" href=\"detail.html\">");
#nullable restore
#line 30 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
                                                           Write(item.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></h6>\r\n                <p class=\"small text-muted\">");
#nullable restore
#line 31 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
                                       Write(item.ABV);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n            </div>\r\n        </div>\r\n");
#nullable restore
#line 34 "C:\Users\stayk\OneDrive\Desktop\BeerOverflow\team10-beeroverflow\Team10-BeerOverflow\BeerOverflow.Web\Views\DrankList\Index.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<BeerOverflow.Web.Models.BeerViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
