﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(BeerOverflow.Web.Areas.Identity.IdentityHostingStartup))]
namespace BeerOverflow.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}