﻿using BeerOverflow.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BeerOverflow.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class LockoutModel : PageModel
    {
        private readonly UserManager<User> _userManager;

        public LockoutModel(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public async void OnGet()
        {
            ViewData["BanDescription"] = "";//(await _userManager.GetUserAsync(User)).BanDescription;
        }
    }
}
