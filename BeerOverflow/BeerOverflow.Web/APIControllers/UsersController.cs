﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery(Name = "sortBy")] string sortBy, [FromQuery(Name = "order")] string order)
        {
            var userList = await _userService.GetMany(sortBy, order);

            return Ok(userList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            UserDTO userDTO = await _userService.Get(id);

            if (userDTO == null)
            {
                return NotFound();
            }

            return Ok(userDTO);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] UserDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            UserDTO userDTO = model;

            await _userService.Create(userDTO);

            return Created("post", userDTO);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            UserDTO userDTO = model;

            await _userService.Update(id, userDTO);

            return Ok(userDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _userService.RemoveFromDb(id));
        }
    }
}
