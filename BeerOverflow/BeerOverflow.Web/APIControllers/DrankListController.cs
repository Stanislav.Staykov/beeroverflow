﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DrankListController : ControllerBase
    {
        private readonly IDrankListService drankListService;

        public DrankListController(IDrankListService drankListService)
        {
            this.drankListService = drankListService;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var drunkList = (await drankListService.GetAllBeerFromUserDrankList(id))
                .Select(x => x.ToViewModel());

            return Ok(drunkList);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromQuery(Name = "userId")] int userId, [FromQuery(Name = "beerId")] int beerId)
        {
            var beerDTO = await drankListService.AddBeerToUserDrankList(userId, beerId);
            return Ok(beerDTO);
        }

        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromQuery(Name = "userId")] int userId, [FromQuery(Name = "beerId")] int beerId)
        {
            return Ok(await drankListService.AddBeerToUserDrankList(userId, beerId));
        }
    }
}
