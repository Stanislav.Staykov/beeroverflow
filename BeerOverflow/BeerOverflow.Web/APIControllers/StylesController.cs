﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StylesController : ControllerBase
    {
        private readonly IStyleService styleService;

        public StylesController(IStyleService styleService)
        {
            this.styleService = styleService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery(Name = "sortBy")] string query, [FromQuery(Name = "order")] string order)
        {
            var styleList = (await styleService.GetMany())
                .Select(x => x.ToViewModel());

            return Ok(styleList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            StyleDTO styleDTO = await styleService.Get(id);

            if (styleDTO == null)
            {
                return NotFound();
            }

            return Ok(styleDTO.ToViewModel());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] StyleDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            StyleDTO styleDTO = model;

            await this.styleService.Create(styleDTO);
            return Created("post", styleDTO);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] StyleDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            StyleDTO styleDTO = model;

            await this.styleService.Update(id, styleDTO);

            return Ok(styleDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await styleService.Delete(id));
        }
    }
}
