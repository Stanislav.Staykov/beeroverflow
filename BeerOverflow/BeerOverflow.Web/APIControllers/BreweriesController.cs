﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BreweriesController : ControllerBase
    {
        private readonly IBreweryService breweryService;

        public BreweriesController(IBreweryService breweryService)
        {
            this.breweryService = breweryService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery(Name = "sortBy")] string query, [FromQuery(Name = "order")] string order)
        {
            var breweryList = (await breweryService.GetMany())
                .Select(x => x.ToViewModel());

            return Ok(breweryList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            BreweryDTO brewery = await this.breweryService.Get(id);

            if (brewery == null)
            {
                return NotFound();
            }

            return Ok(brewery.ToViewModel());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BreweryDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            await this.breweryService.Create(model);

            return Created("post", model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BreweryDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            await this.breweryService.Update(id, model);

            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await breweryService.Delete(id));
        }
    }
}
