﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly ICountryService countryService;

        public CountriesController(ICountryService countryService)
        {
            this.countryService = countryService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery(Name = "sortBy")] string query, [FromQuery(Name = "order")] string order)
        {
            var countryList = (await countryService.GetMany())
                .Select(x => x.ToViewModel());

            return Ok(countryList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            CountryDTO countryDTO = await countryService.Get(id);

            if (countryDTO == null)
            {
                return NotFound();
            }

            return Ok(countryDTO.ToViewModel());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CountryDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            CountryDTO countryDTO = model;

            await this.countryService.Create(countryDTO);

            return Created("post", countryDTO);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CountryDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            CountryDTO countryDTO = model;

            await this.countryService.Update(id, countryDTO);

            return Ok(countryDTO);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await countryService.Delete(id));
        }
    }
}

