﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeersController : ControllerBase
    {
        private readonly IBeerService beerService;

        public BeersController(IBeerService beerService)
        {
            this.beerService = beerService;
        }

        [HttpGet("")]
        public async Task<IActionResult> Get([FromQuery(Name = "sortBy")] string sortQuery = "", [FromQuery(Name = "order")] string order = "",
            [FromQuery(Name = "search")] string searchQuery = "", [FromQuery(Name = "page")] int page = 1,
            [FromQuery(Name = "pageSize")] int pageSize = 20, [FromQuery(Name = "filterBy")] string filterBy = "",
            [FromQuery(Name = "filterValue")] string filterValue = "")
        {
            var beerList = (await beerService.GetMany(filterBy, sortQuery, order, searchQuery, filterValue, pageSize, page))
                .Select(x => x.ToViewModel());

            return Ok(beerList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            BeerDTO beer = await this.beerService.Get(id);

            if (beer == null)
            {
                return NotFound();
            }

            return Ok(beer.ToViewModel());
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] BeerDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            await this.beerService.Create(model);

            return Created("post", model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BeerDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            await this.beerService.Update(id, model);

            return Ok(model);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await beerService.Delete(id));
        }
    }
}
