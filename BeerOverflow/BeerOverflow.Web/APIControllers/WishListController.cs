﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WishListController : ControllerBase
    {
        private readonly IWishListService _wishListService;

        public WishListController(IWishListService wishListService)
        {
            _wishListService = wishListService;
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(int userId)
        {
            var review = (await _wishListService.GetAllBeerFromUserWishList(userId, "", ""))
                .Select(x => x.ToViewModel());

            return Ok(review);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] WishListDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            WishListDTO wishList = model;

            await _wishListService.AddBeerToUserWishList(wishList);

            return Created("post", wishList);
        }

        [HttpPut("{userId}/{beerId}")]
        public async Task<IActionResult> Put(int userId, int beerId, [FromBody] WishListDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            WishListDTO review = model;

            await _wishListService.Update(userId, beerId, review);

            return Ok(review);
        }

        [HttpDelete("{userId}/{beerId}")]
        public async Task<IActionResult> Delete(int userId, int beerId)
        {
            return Ok(await _wishListService.DeleteBeerFromUserWishList(userId, beerId));
        }
    }
}
