﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingsController : ControllerBase
    {
        private readonly IRatingService ratingService;

        public RatingsController(IRatingService ratingService)
        {
            this.ratingService = ratingService;
        }

        [HttpGet("{beerId}")]
        public async Task<IActionResult> Get(int beerId)
        {
            var beerRatings = await ratingService.Get(beerId);

            if (beerRatings == null)
            {
                return NotFound();
            }

            return Ok(beerRatings);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] RatingDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            RatingDTO ratingDTO = model;

            await ratingService.Create(ratingDTO);

            return Created("post", ratingDTO);
        }

        [HttpPut("")]
        public async Task<IActionResult> Put([FromBody] RatingDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            RatingDTO ratingDTO = model;

            await ratingService.Update(ratingDTO);

            return Ok(ratingDTO);
        }

        [HttpDelete("")]
        public async Task<IActionResult> Delete([FromQuery(Name = "UserId")] int userId, [FromQuery(Name = "BeerId")] int beerId)
        {
            return Ok(await ratingService.Delete(userId, beerId));
        }
    }
}
