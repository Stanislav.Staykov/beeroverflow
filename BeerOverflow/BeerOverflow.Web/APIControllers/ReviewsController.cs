﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            ReviewDTO review = await _reviewService.Get(id);

            return Ok(review.ToViewModel());
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var reviews = (await _reviewService.GetAll())
                .Select(x => x.ToViewModel());

            if (reviews == null)
            {
                return NotFound();
            }

            return Ok(reviews);
        }

        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetFromUser(int userId)
        {
            var reviews = (await _reviewService.GetFromUser(userId, "", ""))
                .Select(x => x.ToViewModel());

            if (reviews == null)
            {
                return NotFound();
            }

            return Ok(reviews);
        }

        [HttpGet("beer/{beerId}")]
        public async Task<IActionResult> GetForBeer(int beerId)
        {
            var reviews = (await _reviewService.GetForBeer(beerId, "", ""))
                .Select(x => x.ToViewModel());

            if (reviews == null)
            {
                return NotFound();
            }

            return Ok(reviews);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] ReviewDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            ReviewDTO review = model;

            await _reviewService.Create(review);

            return Created("post", review);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ReviewDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            ReviewDTO review = model;

            await _reviewService.Update(id, review);

            return Ok(review);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await _reviewService.Delete(id));
        }
    }
}
