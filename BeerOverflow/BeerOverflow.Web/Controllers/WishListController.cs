﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    [Authorize(Roles = "User, Admin")]
    public class WishListController : Controller
    {
        private readonly IWishListService _wishListService;

        public WishListController(IWishListService wishListService)
        {
            _wishListService = wishListService;
        }
        // GET: WishListControll
        public async Task<ActionResult> Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var wishListBeersDTO = await _wishListService.GetAllBeerFromUserWishList(int.Parse(userId), null, null);
            var wishListBeersViewModels = wishListBeersDTO.Select(dto => dto.ToViewModel()).ToList();

            return View(wishListBeersViewModels);
        }

        // POST: WishListControll/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(WishListDTO model)
        {
            try
            {
                model.UserID = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

                if (!_wishListService.CheckBeerInUserWishList(model.UserID, model.BeerID))
                {
                    await _wishListService.AddBeerToUserWishList(model);
                }
            }
            catch
            {
            }

            return RedirectToAction("Index");
        }

        // POST: WishListControll/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Remove(WishListDTO model)
        {
            try
            {
                var userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var result = await _wishListService.DeleteBeerFromUserWishList(userId, model.BeerID);
            }
            catch
            {
            }

            return RedirectToAction("Index");
        }
    }
}
