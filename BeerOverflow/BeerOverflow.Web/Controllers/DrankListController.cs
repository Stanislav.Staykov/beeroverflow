﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    [Authorize(Roles = "User, Admin")]
    public class DrankListController : Controller
    {
        private readonly IDrankListService _drankListService;

        public DrankListController(IDrankListService drankListService)
        {
            _drankListService = drankListService;
        }

        // GET: DrankListController
        public async Task<ActionResult> Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var drankListBeersDTO = await _drankListService.GetAllBeerFromUserDrankList(int.Parse(userId));
            var drankListBeersViewModels = drankListBeersDTO.Select(dto => dto.ToViewModel()).ToList();

            return View(drankListBeersViewModels);
        }

        // POST: DrankListController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(DrankListDTO model)
        {
            try
            {
                model.UserID = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

                if (!_drankListService.CheckBeerInUserDrankList(model.UserID, model.BeerID))
                {
                    await _drankListService.AddBeerToUserDrankList(model.UserID, model.BeerID);
                }
            }
            catch
            {
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Remove(DrankListDTO model)
        {
            try
            {
                var userId = int.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));
                var result = await _drankListService.DeleteBeerOfUserDrankList(userId, model.BeerID);
            }
            catch
            {
            }

            return RedirectToAction("Index");
        }
    }
}
