﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<ActionResult> Index()
        {
            return View(
                (await _userService.GetMany(null, null))
                .Select(async x => x.ToViewModel(await _userService.GetRole(x.ID)))
                .Select(t => t.Result));
        }

        public async Task<ActionResult> Details(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _userService.GetRole(id)));
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserDTO userDTO)
        {
            try
            {
                await _userService.Create(userDTO);
            }
            catch
            {
                return View();
            }

            return RedirectToAction(nameof(Index));
        }

        public async Task<ActionResult> Edit(int id)
        {
            var user = await _userService.Get(id);
            user.Role = await _userService.GetRole(id);
            ViewData["Roles"] = new SelectList(await _userService.GetAllRoles());
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, UserDTO userDTO)
        {
            try
            {
                await _userService.Update(id, userDTO);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> Delete(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _userService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, UserDTO user)
        {
            try
            {
                await _userService.RemoveFromDb(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> Ban(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _userService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Ban(int id, UserDTO user)
        {
            try
            {
                string desc = Request.Form["banDescription"];
                await _userService.Ban(id, desc);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<ActionResult> Unban(int id)
        {
            var user = await _userService.Get(id);
            return View(user.ToViewModel(await _userService.GetRole(id)));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Unban(int id, UserDTO user)
        {
            try
            {
                await _userService.Unban(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
