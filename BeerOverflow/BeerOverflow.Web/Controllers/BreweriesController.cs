﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class BreweriesController : Controller
    {
        private readonly IBreweryService _breweryService;
        private readonly ICountryService _countryService;

        public BreweriesController(IBreweryService breweryService, ICountryService countryService)
        {
            _breweryService = breweryService;
            _countryService = countryService;
        }

        public async Task<ActionResult> Index(string filterBy, string filter)
        {
            ViewData["Countries"] = (await _countryService.GetMany()).Select(x => x.Name);
            
            var breweries = await _breweryService.GetMany();

            if (filterBy == "country")
            {
                breweries = breweries.Where(x => x.CountryName == filter).ToList();
            }

            string sortString;
            try
            {
                sortString = Request.Form["sortSelection"].ToString();
            }
            catch
            {
                sortString = "";
            }

            switch (sortString)
            {
                case "name":
                    return View(breweries.Select(x => x.ToViewModel())
                                     .OrderBy(x => x.Name));

                default:
                    return View(breweries.Select(x => x.ToViewModel()));
            }
        }

        public async Task<ActionResult> Details(int id)
        {
            BreweryDTO brewery = await _breweryService.Get(id);
            return View(brewery);
        }

        public async Task<ActionResult> Create()
        {
            ViewData["CountryID"] = new SelectList(await _countryService.GetMany(), "ID", "Name");
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BreweryDTO breweryDTO)
        {
            try
            {
                await _breweryService.Create(breweryDTO);
            }
            catch
            {
                return View();
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int id)
        {
            ViewData["CountryID"] = new SelectList(await _countryService.GetMany(), "ID", "Name");
            BreweryDTO brewery = await _breweryService.Get(id);
            return View(brewery);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, BreweryDTO breweryDTO)
        {
            try
            {
                await _breweryService.Update(id, breweryDTO);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, BreweryDTO breweryDTO)
        {
            try
            {
                await _breweryService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
