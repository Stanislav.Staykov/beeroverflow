﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class StylesController : Controller
    {
        private readonly IStyleService _styleService;

        public StylesController(IStyleService styleService)
        {
            _styleService = styleService;
        }

        public async Task<ActionResult> Index()
        {
            var styles = await _styleService.GetMany();


            string sortString;
            try
            {
                sortString = Request.Form["sortSelection"].ToString();
            }
            catch
            {
                sortString = "";
            }

            switch (sortString)
            {
                case "name":
                    return View(styles.Select(x => x.ToViewModel())
                                     .OrderBy(x => x.Name));

                default:
                    return View(styles.Select(x => x.ToViewModel()));
            }
        }

        public async Task<ActionResult> Details(int id)
        {
            var style = await _styleService.Get(id);
            return View(style);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(StyleDTO styleDTO)
        {
            try
            {
                await _styleService.Create(styleDTO);
            }
            catch
            {
                return View();
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int id)
        {
            var style = await _styleService.Get(id);
            return View(style);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, StyleDTO styleDTO)
        {
            try
            {
                await _styleService.Update(id, styleDTO);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
