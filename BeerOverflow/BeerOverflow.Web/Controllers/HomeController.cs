﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBeerService _beerService;
        private readonly IRatingService _ratingService;

        public HomeController(IBeerService beerService, IRatingService ratingService)
        {
            _beerService = beerService;
            _ratingService = ratingService;
        }

        public async Task<ActionResult> Index()
        {
            var beers = await _beerService.GetMany();

            return View(
                beers.Select(x => x.ToViewModel())
                     .OrderByDescending(x => GetRatingComparator(x.Id))
                     .Take(6));
        }

        private double GetRatingComparator(int id)
        {
            Task<double> result = _ratingService.Average(id, true);
            result.Wait();

            return result.Result;
        }
    }
}
