﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class BeersController : Controller
    {
        private readonly IBeerService _beerService;
        private readonly IStyleService _styleService;
        private readonly IBreweryService _breweryService;
        private readonly ICountryService _countryService;
        private readonly IReviewService _reviewService;
        private readonly IRatingService _ratingService;

        public BeersController(IBeerService beerService, IStyleService styleService,
            IBreweryService breweryService, ICountryService countryService, IReviewService reviewService,
            IRatingService ratingService)
        {
            _beerService = beerService;
            _styleService = styleService;
            _breweryService = breweryService;
            _countryService = countryService;
            _reviewService = reviewService;
            _ratingService = ratingService;
        }

        public async Task<ActionResult> Index(string filterBy, string filter, string sortString = "", int page = 1, string search = "")
        {
            ViewData["Styles"] = (await _styleService.GetMany()).Select(x => x.Name);
            ViewData["Breweries"] = (await _breweryService.GetMany()).Select(x => x.Name);
            ViewData["Countries"] = (await _countryService.GetMany()).Select(x => x.Name);

            return sortString switch
            {
                "name" => View((await _beerService.GetMany(filterBy, sortString, filterValue: filter, page: page, searchQuery:search)
                    ).Select(x => x.ToViewModel())),

                "country" => View((await _beerService.GetMany(filterBy, sortString, filterValue: filter, page: page, searchQuery: search)
                    ).Select(x => x.ToViewModel())),

                "abv-low" => View((await _beerService.GetMany(filterBy, "abv", filterValue: filter, page: page, searchQuery: search)
                    ).Select(x => x.ToViewModel())),

                "abv-high" => View((await _beerService.GetMany(filterBy, "abv", filterValue: filter, page: page, order: "desc", searchQuery: search)
                    ).Select(x => x.ToViewModel())),

                "rating-low" => View((await _beerService.GetMany(filterBy, "rating", filterValue: filter, page: page, searchQuery: search)
                    ).Select(x => x.ToViewModel())),

                "rating-high" => View((await _beerService.GetMany(filterBy, "rating", filterValue: filter, page: page, order: "desc", searchQuery: search)
                    ).Select(x => x.ToViewModel())),

                _ => View((await _beerService.GetMany(filterBy, filterValue: filter, page: page, searchQuery: search)
                    ).Select(x => x.ToViewModel())),
            };
        }

        public async Task<ActionResult> Details(int id)
        {
            var beer = await _beerService.Get(id);
            return View(beer.ToViewModel());
        }

        public async Task<ActionResult> Create()
        {
            ViewData["StyleID"] = new SelectList(await _styleService.GetMany(), "ID", "Name");
            ViewData["BreweryID"] = new SelectList(await _breweryService.GetMany(), "ID", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(BeerDTO beerDTO)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await _beerService.Create(beerDTO);
                }
                catch
                {
                    return RedirectToAction(nameof(Index));
                }
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int id)
        {
            ViewData["StyleID"] = new SelectList(await _styleService.GetMany(), "ID", "Name");
            ViewData["BreweryID"] = new SelectList(await _breweryService.GetMany(), "ID", "Name");

            var beer = await _beerService.Get(id);
            return View(beer);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, BeerDTO beerDTO)
        {
            try
            {
                await _beerService.Update(id, beerDTO);
                return Redirect($"/Beers/Details/{id}");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Review(int userId, int beerId)
        {
            try
            {
                string body = Request.Form["reviewBody"];
                int rating = int.Parse(Request.Form["userRating"]);
                await _reviewService.Create(userId, beerId, body);
                await _ratingService.Create(new RatingDTO { BeerID = beerId, UserID = userId, Value = rating });
            }
            catch
            {
            }

            return Redirect($"Details/{beerId}");
        }

        [HttpPost]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> Like(int reviewId, int beerId)
        {
            try
            {
                await _reviewService.Like(reviewId);
            }
            catch
            {
            }

            return Redirect($"Details/{beerId}");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
