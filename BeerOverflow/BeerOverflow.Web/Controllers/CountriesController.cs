﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Mappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class CountriesController : Controller
    {
        private readonly ICountryService _countryService;

        public CountriesController(ICountryService countryService)
        {
            _countryService = countryService;
        }

        public async Task<ActionResult> Index()
        {
            var countries = await _countryService.GetMany();


            string sortString;
            try
            {
                sortString = Request.Form["sortSelection"].ToString();
            }
            catch
            {
                sortString = "";
            }

            switch (sortString)
            {
                case "name":
                    return View(countries.Select(x => x.ToViewModel())
                                     .OrderBy(x => x.Name));

                default:
                    return View(countries.Select(x => x.ToViewModel()));
            }
        }

        public async Task<ActionResult> Details(int id)
        {
            var country = await _countryService.Get(id);
            return View(country);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(CountryDTO countryDTO)
        {
            try
            {
                await _countryService.Create(countryDTO);
            }
            catch
            {
                return View();
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(int id)
        {
            var country = await _countryService.Get(id);
            return View(country);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, CountryDTO countryDTO)
        {
            try
            {
                await _countryService.Update(id, countryDTO);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, CountryDTO countryDTO)
        {
            try
            {
                await _countryService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
