﻿using BeerOverflow.Services.DTOs;
using BeerOverflow.Web.Models;
using System;
using System.Linq;

namespace BeerOverflow.Web.Mappers
{
    public static class ViewModelMapper
    {
        public static UserViewModel ToViewModel(this UserDTO model, string role)
        {
            return new UserViewModel
            {
                ID = model.ID,
                Name = model.Name,
                Username = model.Username,
                Email = model.Email,
                Role = role,
                IsBanned = model.LockoutEnabled,
                BanDescription = model.BanDescription,
                Age = model.Age
            };
        }

        public static BreweryViewModel ToViewModel(this BreweryDTO model)
        {
            return new BreweryViewModel
            {
                ID = model.ID,
                Name = model.Name,
                CountryName = model.CountryName
            };
        }

        public static CountryViewModel ToViewModel(this CountryDTO model)
        {
            return new CountryViewModel
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static StyleViewModel ToViewModel(this StyleDTO model)
        {
            return new StyleViewModel
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static BeerViewModel ToViewModel(this BeerDTO model)
        {
            return new BeerViewModel
            {
                Id = model.ID,
                Name = model.Name,
                ABV = model.ABV,
                Brewery = model.BreweryName,
                Description = model.Description,
                CountryName = model.CountryName,
                Reviews = model.Reviews.Select(x => x.ToViewModel()).ToList(),
                Style = model.StyleName
            };
        }

        public static ReviewViewModel ToViewModel(this ReviewDTO model)
        {
            return new ReviewViewModel
            {
                ID = model.ID,
                Body = model.Body,
                Name = model.UserName,
                Likes = model.Likes,
                Date = model.Date,
                UserID = Convert.ToInt32(model.UserID)
            };
        }

        public static WishListViewModel ToViewModel(this WishListDTO model)
        {
            return new WishListViewModel
            {
                UserID = model.UserID,
                BeerID = model.BeerID
            };
        } 
        
        public static DrankListViewModel ToViewModel(DrankListDTO model)
        {
            return new DrankListViewModel
            {
                UserID = model.UserID,
                BeerID = model.BeerID
            };
        }
    }
}
