﻿namespace BeerOverflow.Models
{
    public class DrankList
    {
        public int UserId { get; set; }
        public User Users { get; set; }
        public int BeerId { get; set; }
        public virtual Beer Beers { get; set; }
    }
}
