﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Country
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(111)]
        public string Name { get; set; }
        public virtual ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}