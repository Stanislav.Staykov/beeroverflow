﻿using BeerOverflow.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Beer : Entity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(23)]
        public string Name { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(551)]
        public string Description { get; set; }
        [Required]
        public string ABV { get; set; }

        public int? BreweryID { get; set; }
        public Brewery Brewery { get; set; }
        public int? StyleID { get; set; }
        public Style Style { get; set; }
        public int Rating { get; set; }//TODO v otdelen klas?

        public int? ReviewID { get; set; }
        public int? RatingID { get; set; }
        public virtual ICollection<Review> Reviews { get; set; } = new List<Review>();
        public virtual ICollection<Rating> Ratings { get; set; } = new List<Rating>();
        public virtual ICollection<WishList> WishLists { get; set; } = new List<WishList>();
        public virtual ICollection<DrankList> DrankLists { get; set; } = new List<DrankList>();
    }
}