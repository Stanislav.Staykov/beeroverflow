﻿namespace BeerOverflow.Models
{
    public class WishList
    {
        public int UserId { get; set; }
        public User Users { get; set; }
        public int BeerId { get; set; }
        public Beer Beers { get; set; }
    }
}
