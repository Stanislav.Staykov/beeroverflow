﻿using BeerOverflow.Models.Abstract;
using System;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Review : Entity
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(551)]
        public string Body { get; set; }
        public int Likes { get; set; }
        public int? BeerID { get; set; }
        public Beer Beer { get; set; }
        public int? UserID { get; set; }
        public User User { get; set; }
    }
}
