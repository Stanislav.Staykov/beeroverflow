﻿using BeerOverflow.Models.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models

{
    public class Style : Entity
    {
        public int ID { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(111)]
        public string Name { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();//TODO trqbwa li ni
    }
}