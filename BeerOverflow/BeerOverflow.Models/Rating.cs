﻿using BeerOverflow.Models.Abstract;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class Rating : Entity
    {
        //[Key]
        //public int ID { get; set; }
        [Required]
        [Range(1, 5)]
        public int Value { get; set; }
        public int BeerID { get; set; }
        public Beer Beer { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
    }
}
