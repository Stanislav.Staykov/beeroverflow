﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Models
{
    public class User : IdentityUser<int>
    {
        //[Key]
        //public int ID { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(111)]
        public string Name { get; set; }
        [Required]
        [Range(18, 121)]
        public int Age { get; set; }
        public string BanDescription { get; set; }
        public DateTime BannedOn { get; set; }
        public DateTime UnbannedOn { get; set; }
        public int? ReviewID { get; set; }//TODO mai ne ni trqbwat
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public int? RatingID { get; set; }//TODO mai ne ni trqbwat
        public ICollection<Rating> Ratings { get; set; } = new List<Rating>();
        public virtual ICollection<WishList> WishLists { get; set; } = new List<WishList>();//TODO ?
        public virtual ICollection<DrankList> DrankLists { get; set; } = new List<DrankList>();//TODO ?
    }
}