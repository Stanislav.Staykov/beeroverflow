﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class UserService : IUserService
    {
        private readonly DBContext _dbContext;
        private readonly UserManager<User> _userManager;

        public UserService(DBContext dbContext, UserManager<User> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public async Task<string> GetRole(int userId)
        {
            return (await _dbContext.Roles
                .FromSqlRaw("SELECT * FROM AspNetRoles " +
                            "WHERE Id IN " +
                            "(SELECT RoleId FROM AspNetUserRoles " +
                            "WHERE UserId IN " +
                            "(SELECT Id FROM AspNetUsers " +
                            "WHERE Id = {0}))", userId)
                .FirstOrDefaultAsync())
                .Name;
        }

        public async Task<List<string>> GetAllRoles()
        {
            return await _dbContext.Roles
                .Select(x => x.Name)
                .ToListAsync();
        }

        public async Task<UserDTO> Create(UserDTO userDTO)
        {
            User user = userDTO.ToModel();

            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();

            return userDTO;
        }

        public async Task<bool> RemoveFromDb(int id)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return false;
            }

            _dbContext.Users.Remove(user);

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Ban(int id, string description)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return false;
            }

            user.LockoutEnabled = true;
            user.LockoutEnd = DateTime.Now.AddYears(500);
            user.BanDescription = description;
            user.BannedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Unban(int id)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                return false;
            }

            user.LockoutEnabled = false;
            user.UnbannedOn = DateTime.Now;

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<UserDTO> Get(int id)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            UserDTO userDTO = user.ToDTO();

            return userDTO;
        }

        private enum SortString
        {
            ID, Name
        }

        public async Task<ICollection<UserDTO>> GetMany(string sortBy, string order)
        {
            ICollection<UserDTO> userList;

            string orderSanitizedString = (order != "desc") ? "" : " DESC";

            Enum.TryParse(sortBy, out SortString sortSanitizedString);

            userList = await _dbContext.Users
                .FromSqlRaw("SELECT * FROM AspNetUsers ORDER BY " + sortSanitizedString.ToString() + orderSanitizedString)
                //.Where(x => x.IsDeleted == false)
                .Select(x => x.ToDTO())
                .ToListAsync();

            if (userList == null)
            {
                throw new ArgumentNullException();
            }

            return userList;
        }

        public async Task<UserDTO> Update(int id, UserDTO userDTO)
        {
            User user = await _dbContext.Users
                //.Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new ArgumentNullException();
            }

            user.Name = userDTO.Name;
            user.Age = userDTO.Age;

            await _userManager.RemoveFromRoleAsync(user, await this.GetRole(id));
            await _userManager.AddToRoleAsync(user, userDTO.Role);

            await _dbContext.SaveChangesAsync();

            return userDTO;
        }
    }
}
