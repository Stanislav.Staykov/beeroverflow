﻿using BeerOverflow.Database;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class RatingService : IRatingService
    {
        private readonly DBContext _dBContext;

        public RatingService(DBContext dBContext)
        {
            this._dBContext = dBContext;
        }

        public async Task<double> Average(int beerId, bool precise = false)
        {
            var beers = _dBContext.Ratings.Where(x => x.BeerID == beerId);

            return await beers.CountAsync() == 0 ? 0 : precise ? Math.Round(await beers.AverageAsync(x => x.Value), 2) : (int)Math.Floor(await beers.AverageAsync(x => x.Value));
        }

        public async Task<int> UserScore(int beerId, int userId)
        {
            var rating = await _dBContext.Ratings.FirstOrDefaultAsync(x => x.UserID == userId && x.BeerID == beerId);

            return rating == null ? 0 : rating.Value;
        }

        public async Task<int> Count(int beerId)
        {
            return await _dBContext.Ratings.Where(x => x.BeerID == beerId).CountAsync();
        }

        public async Task<bool> Create(RatingDTO model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }
            var ratingToAdd = model.ToModel();

            await _dBContext.Ratings.AddAsync(ratingToAdd);

            await _dBContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> Delete(int userId, int beerId)
        {
            var ratingToDelete = await _dBContext.Ratings.FirstOrDefaultAsync(r => r.UserID == userId & r.BeerID == beerId);
            if (ratingToDelete == null)
            {
                throw new ArgumentNullException();
            }

            _dBContext.Ratings.Remove(ratingToDelete);

            await _dBContext.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<RatingDTO>> Get(int beerId)
        {
            var ratings = await _dBContext.Ratings
                .Where(r => r.BeerID == beerId)
                .Select(rt => rt.ToDTO())
                .ToListAsync();         

            return ratings;
        }

        public async Task<bool> Update(RatingDTO model)
        {
            if (model == null)
            {
                throw new ArgumentNullException();
            }

            var ratingToEdit = await _dBContext.Ratings.FirstAsync(r => r.UserID == model.UserID & r.BeerID == model.BeerID);
            ratingToEdit.Value = model.Value;

            await _dBContext.SaveChangesAsync();

            return true;
        }
    }
}
