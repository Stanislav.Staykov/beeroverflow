﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class WishListService : IWishListService
    {
        private readonly DBContext _dbContext;

        public WishListService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int Count(int userId)
        {
            return _dbContext.WishList.Where(x => x.UserId == userId).Count();
        }

        public async Task<WishListDTO> AddBeerToUserWishList(WishListDTO wishListDTO)
        {
            WishList newWishList = wishListDTO.ToModel();

            if (newWishList == null)
            {
                throw new ArgumentNullException();
            }

            await _dbContext.AddAsync(newWishList);
            await _dbContext.SaveChangesAsync();

            return wishListDTO;
        }

        public async Task<bool> DeleteBeerFromUserWishList(int userId, int beerId)
        {
            WishList toDelete = await _dbContext.WishList.FirstOrDefaultAsync(x => x.BeerId == beerId && x.UserId == userId);

            if (toDelete == null)
            {
                return false;
            }

            _dbContext.Remove(toDelete);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        // TODO: Implement Sorting
        public async Task<ICollection<BeerDTO>> GetAllBeerFromUserWishList(int userId, string sortParameter, string order)
        {
            ICollection<BeerDTO> wishListBeers = await
                _dbContext.WishList
                    .Where(x => x.UserId == userId)
                    .SelectMany(x => _dbContext.Beers.Where(b => b.ID == x.BeerId && b.IsDeleted == false)
                        .Include(b => b.Brewery)
                            .ThenInclude(br => br.Country)
                        .Include(b => b.Style)
                        .Include(b => b.Reviews)
                            .ThenInclude(r => r.User))
                    .Select(x => x.ToDTO())
                    .ToListAsync();

            if (wishListBeers == null)
            {
                throw new ArgumentNullException();
            }

            return wishListBeers;
        }

        public async Task<WishListDTO> Update(int userId, int beerId, WishListDTO wishListDTO)
        {
            WishList toUpdate = await _dbContext.WishList.FirstOrDefaultAsync(x => x.BeerId == beerId && x.UserId == userId);

            if (toUpdate == null)
            {
                return null;
            }

            toUpdate.BeerId = wishListDTO.BeerID;
            toUpdate.UserId = wishListDTO.UserID;

            await _dbContext.SaveChangesAsync();

            return wishListDTO;
        }

        public bool CheckBeerInUserWishList(int userId, int beerId)
        {
            return _dbContext.WishList.Any(w => w.UserId == userId && w.BeerId == beerId);
        }
    }
}
