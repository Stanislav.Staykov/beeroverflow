﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOs;
using System.Linq;

namespace BeerOverflow.Services.Mappers
{
    public static class DTOMapper
    {
        public static BeerDTO ToDTO(this Beer model)
        {
            return new BeerDTO
            {
                ID = model.ID,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                BreweryID = model.BreweryID,
                BreweryName = model.Brewery.Name,
                ReviewID = model.ReviewID,
                StyleID = model.StyleID,
                CountryName = model.Brewery.Country.Name,
                StyleName = model.Style.Name,
                Reviews = model.Reviews.Select(x => x.ToDTO()).ToList()
            };
        }

        public static BreweryDTO ToDTO(this Brewery model)
        {
            return new BreweryDTO
            {
                ID = model.ID,
                Name = model.Name,
                CountryID = model.CountryID,
                CountryName = model.Country.Name
            };
        }

        public static StyleDTO ToDTO(this Style model)
        {
            return new StyleDTO
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static UserDTO ToDTO(this User model)
        {
            return new UserDTO
            {
                ID = model.Id,
                Name = model.Name,
                Age = model.Age,
                Username = model.UserName,
                Email = model.Email,
                BanDescription = model.BanDescription,
                LockoutEnabled = model.LockoutEnabled
            };
        }

        public static CountryDTO ToDTO(this Country model)
        {
            return new CountryDTO
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static WishListDTO ToDTO(this WishList model)
        {
            return new WishListDTO
            {
                UserID = model.UserId,
                BeerID = model.BeerId
            };
        }

        public static DrankListDTO ToDTO(this DrankList model)
        {
            return new DrankListDTO
            {
                UserID = model.UserId,
                BeerID = model.BeerId
            };
        }

        public static ReviewDTO ToDTO(this Review model)
        {
            return new ReviewDTO
            {
                UserID = model.UserID,
                BeerID = model.BeerID,
                Date = model.CreatedOn,
                Likes = model.Likes,
                Body = model.Body,
                UserName = model.User.Name
            };
        }

        public static RatingDTO ToDTO(this Rating model)
        {
            return new RatingDTO
            {
                Value = model.Value,
                UserID = model.UserID,
                BeerID = model.BeerID
            };
        }
    }
}
