﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOs;

namespace BeerOverflow.Services.Mappers
{
    public static class ModelMapper
    {
        public static Beer ToModel(this BeerDTO model)
        {
            return new Beer
            {
                ID = model.ID,
                Name = model.Name,
                Description = model.Description,
                ABV = model.ABV,
                ReviewID = model.ReviewID,
                StyleID = model.StyleID,
                BreweryID = model.BreweryID
            };
        }

        public static Brewery ToModel(this BreweryDTO model)
        {
            return new Brewery
            {
                ID = model.ID,
                Name = model.Name,
                CountryID = model.CountryID
            };
        }

        public static Style ToModel(this StyleDTO model)
        {
            return new Style
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static User ToModel(this UserDTO model)
        {
            return new User
            {
                //ID = model.ID,
                Name = model.Name,
                Age = model.Age
            };
        }

        public static Country ToModel(this CountryDTO model)
        {
            return new Country
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public static Rating ToModel(this RatingDTO model)
        {
            return new Rating
            {
                Value = model.Value,
                UserID = model.UserID,
                BeerID = model.BeerID
            };
        }

        public static WishList ToModel(this WishListDTO model)
        {
            return new WishList
            {
                UserId = model.UserID,
                BeerId = model.BeerID
            };
        }

        public static Review ToModel(this ReviewDTO model)
        {
            return new Review
            {
                //UserID = model.UserID,
                BeerID = model.BeerID,
                //ID = model.ID,
                Body = model.Body
            };
        }
    }
}
