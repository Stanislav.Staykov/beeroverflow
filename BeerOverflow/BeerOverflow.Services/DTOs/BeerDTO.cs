﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeerOverflow.Services.DTOs
{
    public class BeerDTO
    {
        public int ID { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 5)]
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<ReviewDTO> Reviews { get; set; }
        public string StyleName { get; set; }
        public string CountryName { get; set; }
        public int? CountryID { get; set; }
        public string BreweryName { get; set; }
        public int? BreweryID { get; set; }
        public int? StyleID { get; set; }
        public int? ReviewID { get; set; }
        public int? RatingID { get; set; }
        public string ABV { get; set; }
    }
}
