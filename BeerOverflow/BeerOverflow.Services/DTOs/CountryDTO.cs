﻿namespace BeerOverflow.Services.DTOs
{
    public class CountryDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
