﻿namespace BeerOverflow.Services.DTOs
{
    public class WishListDTO
    {
        public int UserID { get; set; }
        public int BeerID { get; set; }
    }
}
