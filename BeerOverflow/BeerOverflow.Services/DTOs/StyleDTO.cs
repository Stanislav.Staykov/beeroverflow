﻿namespace BeerOverflow.Services.DTOs
{
    public class StyleDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
