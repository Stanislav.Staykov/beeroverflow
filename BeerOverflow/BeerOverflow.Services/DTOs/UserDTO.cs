﻿namespace BeerOverflow.Services.DTOs
{
    public class UserDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string BanDescription { get; set; }
        public bool LockoutEnabled { get; set; }
    }
}
