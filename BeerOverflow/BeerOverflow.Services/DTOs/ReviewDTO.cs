﻿using System;

namespace BeerOverflow.Services.DTOs
{
    public class ReviewDTO
    {
        public int ID { get; set; }
        public string Body { get; set; }
        public string UserName { get; set; }
        public int Likes { get; set; }
        public DateTime Date { get; set; }
        public int? BeerID { get; set; }
        public int? UserID { get; set; }
    }
}
