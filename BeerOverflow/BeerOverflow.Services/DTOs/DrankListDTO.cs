﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOs
{
    public class DrankListDTO
    {
        public int UserID { get; set; }
        public int BeerID { get; set; }
    }
}
