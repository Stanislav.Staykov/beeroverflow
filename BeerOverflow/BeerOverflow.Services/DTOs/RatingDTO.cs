﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOs
{
    public class RatingDTO
    {
        public int Value { get; set; }
        public int BeerID { get; set; }
        public int UserID { get; set; }
    }
}
