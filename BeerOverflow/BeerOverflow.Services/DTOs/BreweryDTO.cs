﻿namespace BeerOverflow.Services.DTOs
{
    public class BreweryDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? CountryID { get; set; }
        public string CountryName { get; set; }
    }
}
