﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class StyleService : IStyleService
    {
        private readonly DBContext _dBContext;

        public StyleService(DBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<StyleDTO> Create(StyleDTO styleDTO)
        {
            Style newStyle = styleDTO.ToModel();

            Style oldStyle = await _dBContext.Styles
                .Where(x => x.Name == styleDTO.Name)
                .FirstOrDefaultAsync();

            if (oldStyle == null)
            {
                await _dBContext.AddAsync(newStyle);
                await _dBContext.SaveChangesAsync();
            }
            else if (oldStyle.IsDeleted == true)
            {
                oldStyle.IsDeleted = false;
                await _dBContext.SaveChangesAsync();
            }

            return styleDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                Style style = await _dBContext.Styles.FirstOrDefaultAsync(s => s.ID == id);

                style.IsDeleted = true;
                style.DeletedOn = DateTime.Now;

                await _dBContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<StyleDTO> Get(int id)
        {
            Style style = await _dBContext.Styles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(s => s.ID == id);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            StyleDTO styleDTO = style.ToDTO();

            return styleDTO;
        }

        public async Task<IEnumerable<StyleDTO>> GetMany()
        {
            IEnumerable<StyleDTO> styleList = new List<StyleDTO>();

            styleList = await _dBContext.Styles
                .Where(s => s.IsDeleted == false)
                .Select(s => s.ToDTO())
                .Take(20)
                .ToListAsync();

            if (styleList == null)
            {
                throw new ArgumentNullException();
            }

            return styleList;
        }

        public async Task<StyleDTO> Update(int id, StyleDTO styleDTO)
        {
            Style style = await _dBContext.Styles
                .Where(s => s.IsDeleted == false)
                .FirstOrDefaultAsync(s => s.ID == id);

            if (style == null)
            {
                throw new ArgumentNullException();
            }

            style.ID = styleDTO.ID;
            style.Name = styleDTO.Name;

            await _dBContext.SaveChangesAsync();

            return styleDTO;
        }
    }
}
