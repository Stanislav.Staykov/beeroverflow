﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class BreweryService : IBreweryService
    {
        private readonly DBContext _dbContext;

        public BreweryService(DBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<BreweryDTO> Create(BreweryDTO brewery)
        {
            Brewery oldBrewery = await _dbContext.Breweries
                .Where(x => x.Name == brewery.Name && x.CountryID == brewery.CountryID)
                .FirstOrDefaultAsync();

            if (oldBrewery == null)
            {
                await _dbContext.AddAsync(brewery.ToModel());
                await _dbContext.SaveChangesAsync();
            }
            else if (oldBrewery.IsDeleted == true)
            {
                oldBrewery.IsDeleted = false;
                await _dbContext.SaveChangesAsync();
            }

            return brewery;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                Brewery breweryToRemove = await _dbContext.Breweries
                    .Where(x => x.IsDeleted == false)
                    .FirstOrDefaultAsync(brewery => brewery.ID == id);

                breweryToRemove.IsDeleted = true;
                breweryToRemove.DeletedOn = DateTime.Now;

                await _dbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BreweryDTO> Get(int id)
        {
            Brewery brewery = await _dbContext.Breweries
                .Where(x => x.IsDeleted == false)
                .Include(x => x.Country)
                .FirstOrDefaultAsync(brewery => brewery.ID == id);

            BreweryDTO breweryDTO = brewery.ToDTO();

            if (breweryDTO == null)
            {
                throw new ArgumentNullException();
            }

            return breweryDTO;
        }

        public async Task<ICollection<BreweryDTO>> GetMany()
        {
            ICollection<BreweryDTO> breweries = await _dbContext.Breweries
                .Include(x => x.Country)
                .Select(x => x.ToDTO())
                .ToListAsync();

            if (breweries == null)
            {
                throw new ArgumentNullException();
            }

            return breweries;
        }

        public async Task<BreweryDTO> Update(int id, BreweryDTO breweryDTO)
        {
            Brewery brewery = await _dbContext.Breweries
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(b => b.ID == id);

            if (brewery == null)
            {
                throw new ArgumentNullException();
            }

            brewery.CountryID = breweryDTO.CountryID;
            brewery.Name = breweryDTO.Name;

            await _dbContext.SaveChangesAsync();

            return breweryDTO;
        }
    }
}
