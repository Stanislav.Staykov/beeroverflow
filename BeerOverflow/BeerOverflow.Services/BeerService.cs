﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class BeerService : IBeerService
    {
        private readonly DBContext _dbContext;
        private readonly IRatingService _ratingService;

        public BeerService(DBContext dbContext, IRatingService ratingService)
        {
            _dbContext = dbContext;
            _ratingService = ratingService;
        }

        public async Task<BeerDTO> Create(BeerDTO beerDTO)
        {
            Beer oldBeer = await _dbContext.Beers
                .Where(x => x.Name == beerDTO.Name && x.BreweryID == beerDTO.BreweryID)
                .FirstOrDefaultAsync();

            if (oldBeer == null)
            {
                await _dbContext.Beers.AddAsync(beerDTO.ToModel());
            }
            else if (oldBeer.IsDeleted)
            {
                oldBeer.IsDeleted = false;
            }

            await _dbContext.SaveChangesAsync();
            return beerDTO;
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                Beer beer = await _dbContext.Beers
                    .FirstOrDefaultAsync(x => x.ID == id);

                beer.IsDeleted = true;
                beer.DeletedOn = DateTime.Now;

                _dbContext.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<BeerDTO> Get(int id)
        {
            Beer beer = await _dbContext.Beers
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Include(x => x.Reviews)
                    .ThenInclude(r => r.User)
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.ID == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            BeerDTO beerDTO = beer.ToDTO();

            return beerDTO;
        }

        public async Task<ICollection<BeerDTO>> GetMany()
        {
            ICollection<BeerDTO> beerList = await _dbContext.Beers
                .Where(x => x.IsDeleted == false)
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Select(x => x.ToDTO())
                .ToListAsync();

            if (beerList == null)
            {
                throw new ArgumentNullException();
            }

            return beerList;
        }

        public async Task<ICollection<BeerDTO>> GetMany(string filterBy, string sortBy = "id", string order = "asc", 
            string searchQuery = "", string filterValue = "", int pageSize = 20, int page = 1)
        {
            IEnumerable<BeerDTO> beerList = (await FilterBeers(filterBy, filterValue))
                .Where(x => x.Name.Contains(searchQuery, StringComparison.OrdinalIgnoreCase))
                .OrderBy(OrderBySelector(sortBy));

            if (order == "desc")
            {
                beerList = beerList.Reverse();
            }

            if (beerList == null)
            {
                throw new ArgumentNullException();
            }

            return beerList
                .Skip(pageSize * (page < 1 ? 0 : page - 1))
                .Take(pageSize)
                .ToList();
        }

        private Func<BeerDTO, object> OrderBySelector(string orderBy) =>
            orderBy.ToLower() switch
            {
                "name" => x => x.Name,
                "country" => x => x.CountryName,
                "rating" => x => GetRatingComparator(x.ID),
                "id" => x => x.ID,
                "abv" => x => x.ABV,
                "description" => x => x.Description,
            
                _ => x => x.ID
            };

        private double GetRatingComparator(int id)
        {
            Task<double> result = _ratingService.Average(id, true);
            result.Wait();

            return result.Result;
        }

        public async Task<BeerDTO> Update(int id, BeerDTO beerDTO)
        {
            Beer beer = await _dbContext.Beers
                .FirstOrDefaultAsync(x => x.ID == id);

            if (beer == null)
            {
                throw new ArgumentNullException();
            }

            beer.Name = beerDTO.Name;
            beer.ReviewID = beerDTO.ReviewID;
            beer.StyleID = beerDTO.StyleID;
            beer.BreweryID = beerDTO.BreweryID;
            beer.Description = beerDTO.Description;
            beer.ABV = beerDTO.ABV;

            await _dbContext.SaveChangesAsync();

            return beerDTO;
        }

        private async Task<IEnumerable<BeerDTO>> FilterBeers(string filterBy, string filterValue = "") =>
        filterBy switch
        {
            "country" => await _dbContext.Beers
                .FromSqlRaw("SELECT * FROM Beers " +
                            "WHERE BreweryID IN " +
                            "(SELECT ID FROM Breweries " +
                            "WHERE CountryID IN " +
                            "(SELECT ID FROM Countries " +
                            "WHERE Name = {0}))", filterValue)
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Select(x => x.ToDTO())
                .ToListAsync(),

            "style" => await _dbContext.Beers
                .FromSqlRaw("SELECT * FROM Beers " +
                            "WHERE StyleID IN " +
                            "(SELECT ID FROM Styles " +
                            "WHERE Name = {0})", filterValue)
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Select(x => x.ToDTO())
                .ToListAsync(),

            "brewery" => await _dbContext.Beers
                .FromSqlRaw("SELECT * FROM Beers " +
                            "WHERE BreweryID IN " +
                            "(SELECT ID FROM Breweries " +
                            "WHERE Name = {0})", filterValue)
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Select(x => x.ToDTO())
                .ToListAsync(),

            _ => await _dbContext.Beers
                .Include(x => x.Brewery)
                    .ThenInclude(b => b.Country)
                .Include(x => x.Style)
                .Select(x => x.ToDTO())
                .ToListAsync(),
        };
    }
}