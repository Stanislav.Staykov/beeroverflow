﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class ReviewService : IReviewService
    {
        private readonly DBContext _dBContext;

        public ReviewService(DBContext dBContext)
        {
            _dBContext = dBContext;
        }

        public async Task<ReviewDTO> Create(ReviewDTO reviewDTO)
        {
            Review toAdd = reviewDTO.ToModel();

            if (toAdd == null)
            {
                throw new ArgumentNullException();
            }

            toAdd.Likes = 0;

            await _dBContext.AddAsync(toAdd);
            await _dBContext.SaveChangesAsync();

            return reviewDTO;
        }

        public async Task<bool> Like(int reviewId)
        {
            Review toUpdate = await _dBContext.Reviews
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.ID == reviewId);

            if (toUpdate == null)
            {
                return false;
            }

            toUpdate.Likes += 1;

            await _dBContext.SaveChangesAsync();

            return true;
        }

        public async Task<ReviewDTO> Create(int userId, int beerId, string body)
        {
            Review toAdd = new Review
            {
                Body = body,
                BeerID = beerId,
                UserID = userId
            };

            await _dBContext.AddAsync(toAdd);
            await _dBContext.SaveChangesAsync();

            return new ReviewDTO { Body = body, BeerID = beerId, UserID = userId};
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                Review toDelete = await _dBContext.Reviews
                    .Where(x => x.IsDeleted == false)
                    .FirstOrDefaultAsync(x => x.ID == id);

                if (toDelete == null)
                {
                    return false;
                }

                // TODO: Add DeletedOn.DateTimeProvider
                toDelete.IsDeleted = true;
                toDelete.DeletedOn = DateTime.Now;

                await _dBContext.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<ReviewDTO> Get(int id)
        {
            Review review = await _dBContext.Reviews
                .Where(x => x.IsDeleted == false)
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.ID == id);

            return review.ToDTO();
        }

        public async Task<ICollection<ReviewDTO>> GetAll()
        {
            return await _dBContext.Reviews
                .Where(x => x.IsDeleted == false)
                .Include(x => x.User)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<ICollection<ReviewDTO>> GetFromUser(int userId, string sortParameter, string order)
        {
            // TODO: Implement Review Sorting

            return await _dBContext.Reviews
                //.Where(x => x.UserID == userId && x.IsDeleted == false)
                .Include(x => x.User)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<ICollection<ReviewDTO>> GetForBeer(int beerId, string sortParameter, string order)
        {
            // TODO: Implement Review Sorting

            return await _dBContext.Reviews
                .Where(x => x.BeerID == beerId && x.IsDeleted == false)
                .Include(x => x.User)
                .Select(x => x.ToDTO())
                .ToListAsync();
        }

        public async Task<ReviewDTO> Update(int id, ReviewDTO reviewDTO)
        {
            Review toUpdate = await _dBContext.Reviews
                .Where(x => x.IsDeleted == false)
                .FirstOrDefaultAsync(x => x.ID == id);

            if (toUpdate == null)
            {
                return null;
            }

            toUpdate.Body = reviewDTO.Body;
            
            await _dBContext.SaveChangesAsync();

            return reviewDTO;
        }
    }
}
