﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IRatingService
    {
        Task<ICollection<RatingDTO>> Get(int beerId);       
        Task<bool> Create(RatingDTO model);
        Task<bool> Update(RatingDTO model);//(int userId, int beerId, int value);
        Task<bool> Delete(int userId, int beerId);
        public Task<double> Average(int beerId, bool precise = false);
        public Task<int> UserScore(int beerId, int userId);
        public Task<int> Count(int beerId);
    }
}
