﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface ICountryService
    {
        Task<CountryDTO> Get(int id);
        Task<IEnumerable<CountryDTO>> GetMany();
        Task<CountryDTO> Create(CountryDTO countryDTO);
        Task<CountryDTO> Update(int id, CountryDTO countryDTO);
        Task<bool> Delete(int id);
    }
}
