﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IReviewService
    {
       Task<ReviewDTO> Get(int id);
       Task<ICollection<ReviewDTO>> GetAll();
       Task<ICollection<ReviewDTO>> GetFromUser(int userId, string sortParameter, string order);
       Task<ICollection<ReviewDTO>> GetForBeer(int beerId, string sortParameter, string order);
       Task<ReviewDTO> Create(ReviewDTO reviewDTO);
       Task<ReviewDTO> Create(int userId, int beerId, string body);
       Task<ReviewDTO> Update(int id, ReviewDTO reviewDTO);
       Task<bool> Like(int reviewId);
       Task<bool> Delete(int id);
    }
}
