﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IStyleService
    {
        Task<StyleDTO> Get(int id);
        Task<IEnumerable<StyleDTO>> GetMany();
        Task<StyleDTO> Create(StyleDTO styleDTO);
        Task<StyleDTO> Update(int id, StyleDTO styleDTO);
        Task<bool> Delete(int id);
    }
}
