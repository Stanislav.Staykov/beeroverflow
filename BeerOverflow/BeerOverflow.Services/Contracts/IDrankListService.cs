﻿using BeerOverflow.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IDrankListService
    {
        Task<DrankListDTO> AddBeerToUserDrankList(int userId, int beerId);
        Task<bool> DeleteBeerOfUserDrankList(int userId, int beerId);
        Task<ICollection<BeerDTO>> GetAllBeerFromUserDrankList(int userId);
        bool CheckBeerInUserDrankList(int userId, int beerId);
        public int Count(int userId);
    }
}
