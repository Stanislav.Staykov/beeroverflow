﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IBeerService
    {
        Task<BeerDTO> Get(int id);
        Task<ICollection<BeerDTO>> GetMany();
        Task<ICollection<BeerDTO>> GetMany(string filterBy, string sortBy = "id", string order = "asc",
            string searchQuery = "", string filterValue = "", int pageSize = 20, int page = 1);
        Task<BeerDTO> Create(BeerDTO beerDTO);
        Task<BeerDTO> Update(int id, BeerDTO beerDTO);
        Task<bool> Delete(int id);
    }
}
