﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IBreweryService
    {
        Task<BreweryDTO> Get(int id);
        Task<ICollection<BreweryDTO>> GetMany();
        Task<BreweryDTO> Create(BreweryDTO beerDTO);
        Task<BreweryDTO> Update(int id, BreweryDTO beerDTO);
        Task<bool> Delete(int id);
    }
}
