﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IUserService
    {
        Task<UserDTO> Get(int id);
        Task<ICollection<UserDTO>> GetMany(string sortParameter, string order);
        Task<UserDTO> Create(UserDTO beerDTO);
        Task<UserDTO> Update(int id, UserDTO beerDTO);
        Task<bool> RemoveFromDb(int id);
        Task<bool> Ban(int id, string description);
        Task<bool> Unban(int id);
        Task<string> GetRole(int userId);
        Task<List<string>> GetAllRoles();
    }
}
