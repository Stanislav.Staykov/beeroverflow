﻿using BeerOverflow.Services.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Contracts
{
    public interface IWishListService
    {
        Task<ICollection<BeerDTO>> GetAllBeerFromUserWishList(int userId, string sortParameter, string order);
        Task<WishListDTO> AddBeerToUserWishList(WishListDTO wishListDTO);
        Task<WishListDTO> Update(int userId, int beerId, WishListDTO wishListDTO);
        Task<bool> DeleteBeerFromUserWishList(int userId, int beerId);
        bool CheckBeerInUserWishList(int userId, int beerId);
        public int Count(int userId);
    }
}
