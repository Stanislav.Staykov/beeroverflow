﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class DrankListService : IDrankListService
    {
        private readonly DBContext _dBContext;

        public DrankListService(DBContext dBContext)
        {
            this._dBContext = dBContext;
        }

        public int Count(int userId)
        {
            return _dBContext.DrankList.Where(x => x.UserId == userId).Count();
        }

        public async Task<DrankListDTO> AddBeerToUserDrankList(int userId, int beerId)
        {
            if (_dBContext.Beers.FirstOrDefault(br => br.ID == beerId) == null)
            {
                throw new ArgumentNullException();
            }

            var item = new DrankList { UserId = userId, BeerId = beerId };
            await _dBContext.DrankList.AddAsync(item);

            await _dBContext.SaveChangesAsync();
            return item.ToDTO();
        }

        public async Task<bool> DeleteBeerOfUserDrankList(int userId, int beerId)
        {
            var deleteBeer = await _dBContext.DrankList.Where(d => d.BeerId == beerId & d.UserId == userId).FirstOrDefaultAsync();

            _dBContext.DrankList.Remove(deleteBeer);
            await _dBContext.SaveChangesAsync();

            return true;
        }

        public async Task<ICollection<BeerDTO>> GetAllBeerFromUserDrankList(int userId)
        {
            ICollection<BeerDTO> drankListBeers = await
                _dBContext.DrankList
                    .Where(x => x.UserId == userId)
                    .SelectMany(x => _dBContext.Beers.Where(b => b.ID == x.BeerId && b.IsDeleted == false)
                        .Include(b => b.Brewery)
                            .ThenInclude(br => br.Country)
                        .Include(b => b.Style)
                        .Include(b => b.Reviews)
                            .ThenInclude(r => r.User))
                    .Select(x => x.ToDTO())
                    .ToListAsync();

            return drankListBeers;
        }

        public bool CheckBeerInUserDrankList(int userId, int beerId)
        {
            return _dBContext.DrankList.Any(d => d.UserId == userId && d.BeerId == beerId);
        }
    }
}
