﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Services
{
    public class CountryService : ICountryService
    {
        private readonly DBContext dbContext;

        public CountryService(DBContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public async Task<CountryDTO> Create(CountryDTO countryDTO)
        {
            Country country = countryDTO.ToModel();

            await dbContext.Countries.AddAsync(country);
            await this.dbContext.SaveChangesAsync();

            return countryDTO;
        }

        public async Task<bool> Delete(int id)
        {
            Country country = await dbContext.Countries
                .FirstOrDefaultAsync(c => c.ID == id);

            if (country == null)
            {
                return false;
            }

            dbContext.Countries.Remove(country);

            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<CountryDTO> Get(int id)
        {
            Country country = await dbContext.Countries.FirstOrDefaultAsync(c => c.ID == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            CountryDTO countryDTO = country.ToDTO();

            return countryDTO;
        }

        public async Task<IEnumerable<CountryDTO>> GetMany()
        {
            IEnumerable<CountryDTO> countryList = new List<CountryDTO>();

            countryList = await dbContext.Countries
                                   .Select(c => c.ToDTO())
                                   .Take(20)
                                   .ToListAsync();

            if (countryList == null)
            {
                throw new ArgumentNullException();
            }

            return countryList;
        }

        public async Task<CountryDTO> Update(int id, CountryDTO countryDTO)
        {
            Country country = await dbContext.Countries.FirstOrDefaultAsync(c => c.ID == id);

            if (country == null)
            {
                throw new ArgumentNullException();
            }

            country.ID = countryDTO.ID;
            country.Name = countryDTO.Name;

            await this.dbContext.SaveChangesAsync();

            return countryDTO;
        }
    }
}
