﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.BreweryServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnNewBrewery_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnNewBrewery_When_ParamsAreValid));

            var brewery = new Brewery
            {
                ID = 1,
                Name = "London Brewery",
                CountryID = 1,
                CreatedOn = DateTime.Parse("10/10/2010")
            };

            //var providerMock = new Mock<IDateTimeProvider>();

            //var temp = new Mock<IBeerService>();

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                var country = new Country
                {
                    ID = 1,
                    Name = "BG"
                };
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new BreweryService(actContext);
                //var result = actContext.Breweries.FirstOrDefault(br => br.ID == 1); //TODO we shoult test Get()
                var result = await sut.Get(1);

                //Assert
                Assert.AreEqual(brewery.ID, result.ID);
                Assert.AreEqual(brewery.Name, result.Name);
                Assert.AreEqual(brewery.CountryID, result.CountryID);
            }
        }

        [TestMethod]
        public void Throw_When_BreweryNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_BreweryNotFound));
            //var providerMock = new Mock<IDateTimeProvider>();

            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new BreweryService(actContext);

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Get(1));
            }
        }
    }
}
