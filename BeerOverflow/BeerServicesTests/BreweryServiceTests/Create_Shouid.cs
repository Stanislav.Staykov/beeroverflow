﻿using BeerOverflow.Database;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.BreweryServiceTests
{
    [TestClass]
    public class Create_Shouid
    {
        [TestMethod]
        public async Task ReturnBrewery_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnBrewery_When_ParamsAreValid));

            var breweryDTO = new BreweryDTO
            {
                ID = 1,
                Name = "London Brewery",
                CountryID = 1               
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new BreweryService(arrangeContext);
                var newBeer = await sut.Create(breweryDTO);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new BreweryService(assertContext);

                //Act               
                var actual = assertContext.Breweries.FirstOrDefault(b => b.ID == 1);

                //Assert
                Assert.AreEqual(breweryDTO.Name, actual.Name);
               
            }
        }
    }
}
