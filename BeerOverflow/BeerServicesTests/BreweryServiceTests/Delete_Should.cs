﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.BreweryServiceTests
{
    [TestClass]
    public class Delete_should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
            //var options = new DbContextOptionsBuilder<DbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            //var providerMock = new Mock<IDateTimeProvider>();

            var brewery = new Brewery
            {
                ID = 1,
                Name = "London Brewery",
                CountryID = 1,
                CreatedOn = DateTime.Parse("10/10/2010")
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new BreweryService(assertContext);

                //Act
                var result = await sut.Delete(1);
                var actual = assertContext.Breweries.FirstOrDefault(b => b.ID == 1);
                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actual.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new BreweryService(assertContext);

                //Act
                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public void CorrectlySetDeleteValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));
            //var providerMock = new Mock<IDateTimeProvider>();

            //providerMock
            //    .Setup(x => x.GetDateTime())
            //    .Returns(DateTime.Parse("10, 10, 2020"));

            var brewery = new Brewery
            {
                ID = 1,
                Name = "London Brewery",
                CountryID = 1,
                CreatedOn = DateTime.Parse("10/10/2010")
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new BreweryService(actContext);
                _ = sut.Delete(1);
            }

            using (var assertContext = new DBContext(options))
            {
                var actual = assertContext.Breweries.First(x => x.ID == 1);

                Assert.IsTrue(actual.IsDeleted);
                // Assert.AreEqual(DateTime.Now, actual.DeletedOn);
            }
        }
    }
}
