using BeerOverflow.Database;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices.ComTypes;

namespace BeerServicesTests
{   
    public class Utils
    {
        public static DbContextOptions<DBContext> GetOptions(string databaseName)
        {
          return  new DbContextOptionsBuilder<DBContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;         
        }
    }
}
