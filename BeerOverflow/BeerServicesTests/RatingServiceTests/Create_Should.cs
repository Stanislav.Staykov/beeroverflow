﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.RatingServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnRating_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnRating_When_ParamsAreValid));

            var ratingDTO = new RatingDTO
            {
                Value = 5,
                UserID = 1,
                BeerID = 1
            };

            using (DBContext arrangeContext = new DBContext(options))
            {
                var sut = new RatingService(arrangeContext);
                var newBeer = await sut.Create(ratingDTO);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new RatingService(assertContext);

                //Act               
                var actual = assertContext.Ratings.FirstOrDefault(r => r.UserID==1 & r.BeerID==1);

                //Assert
                Assert.AreEqual(ratingDTO.Value,5);

            }
        }
    }
}
