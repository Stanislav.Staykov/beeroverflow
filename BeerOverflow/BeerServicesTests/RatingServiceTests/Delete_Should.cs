﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.RatingServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
            //var options = new DbContextOptionsBuilder<DbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            //var providerMock = new Mock<IDateTimeProvider>();

            var rating = new Rating
            {
                Value = 5,
                UserID = 1,
                BeerID = 1
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Ratings.Add(rating);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new RatingService(assertContext);

                //Act
                var result = await sut.Delete(1, 1);

                ////Assert
                Assert.IsTrue(result);
                //Assert.IsTrue(country.IsDeleted);
            }
        }

        [TestMethod]
        public void ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new RatingService(assertContext);

                //Act
                //var result = await sut.Delete(1, 2);

                //Assert
                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Delete(1, 2));
            }
        }

        [TestMethod]
        public async Task CorrectlySetDeleteValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));
            //var providerMock = new Mock<IDateTimeProvider>();

            //providerMock
            //    .Setup(x => x.GetDateTime())
            //    .Returns(DateTime.Parse("10, 10, 2020"));

            var rating = new Rating
            {
                Value = 5,
                UserID = 1,
                BeerID = 1
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Ratings.Add(rating);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new RatingService(actContext);
                _ =await sut.Delete(1,1);
            }

            using (var assertContext = new DBContext(options))
            {
                var actual = assertContext.Ratings.FirstOrDefault(x => x.UserID == 1 & x.BeerID==1);

                Assert.IsNull(actual);
                // Assert.AreEqual(DateTime.Now, actual.DeletedOn);
            }
        }
    }
}
