﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.RatingServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void  ReturnRating_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnRating_When_ParamsAreValid));

            var rating = new Rating
            {
                Value = 5,
                UserID = 1,
                BeerID = 1
            };
            
            var user = new User
            {
                Id = 1,                
                UserName="Ivan"
            };
           
            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Ratings.Add(rating);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new RatingService(actContext);
                var result = sut.Get(1);

                //Assert
                Assert.AreEqual(rating.Value, 5);              
            }
        }       
    }
}
