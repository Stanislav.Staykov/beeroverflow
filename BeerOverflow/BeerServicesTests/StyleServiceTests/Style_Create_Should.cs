﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using BeerOverflow.Services.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.StyleServiceTests
{
    [TestClass]
    public class Style_Create_Should
    {
        [TestMethod]
        public async Task ReturnStyle_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnStyle_When_ParamsAreValid));

            var styleDTO = new Style
            {
                Name = "American Imperial Porter"
            };

            using (DBContext arrangeContext = new DBContext(options))
            {
                var sut = new StyleService(arrangeContext);
                var newBeer = await sut.Create(styleDTO.ToDTO());
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new RatingService(assertContext);

                //Act               
                var actual = assertContext.Ratings.FirstOrDefault(r => r.UserID == 1 & r.BeerID == 1);

                //Assert
                Assert.AreEqual(styleDTO.Name, "American Imperial Porter");
                Assert.IsFalse(styleDTO.IsDeleted);

            }
        }
    }
}
