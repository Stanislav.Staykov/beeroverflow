﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.StyleServiceTests
{
    [TestClass]
    public class Style_Get_Should
    {

        [TestMethod]
        public async Task ReturnReview_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnReview_When_ParamsAreValid));

            var style = new Style
            {
                ID = 1,
                Name = "Dark Moon"                
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Styles.Add(style);               
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new StyleService(actContext);
                var result = await sut.Get(1);

                //Assert
                Assert.AreEqual(style.Name, "Dark Moon");
                Assert.IsFalse(style.IsDeleted);
            }
        }
    }
}
