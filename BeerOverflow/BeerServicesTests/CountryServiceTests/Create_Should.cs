﻿using BeerOverflow.Database;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.CountryServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnCountry_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCountry_When_ParamsAreValid));

            var countryDTO = new CountryDTO
            {
                ID = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new CountryService(arrangeContext);
                var newBeer = await sut.Create(countryDTO);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new BreweryService(assertContext);

                //Act               
                var actual = assertContext.Countries.FirstOrDefault(b => b.ID == 1);

                //Assert
                Assert.AreEqual(countryDTO.Name, actual.Name);

            }
        }
    }
}
