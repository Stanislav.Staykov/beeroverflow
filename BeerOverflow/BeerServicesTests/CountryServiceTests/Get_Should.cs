﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.CountryServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnNewCountry_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnNewCountry_When_ParamsAreValid));

            var country = new Country
            {
                ID = 1,
                Name = "UK"               
            };           

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);
                var result = await sut.Get(1);

                //Assert
                Assert.AreEqual(country.ID, result.ID);
                Assert.AreEqual(country.Name, result.Name);
            }
        }
        [TestMethod]
        public async Task ReturnNewCountries_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnNewCountries_When_ParamsAreValid));

            var country = new Country
            {
                ID = 1,
                Name = "UK"
            }; 
            var country1 = new Country
            {
                ID = 2,
                Name = "Belgium"
            };
            var country2 = new Country
            {
                ID = 3,
                Name = "Bulgaria"
            };

            //var providerMock = new Mock<IDateTimeProvider>();

            //var temp = new Mock<IBeerService>();

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.Countries.Add(country1);
                arrangeContext.Countries.Add(country2);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);
                var result = await sut.GetMany();
                //var result1 = await sut.Get(2);
                //var result2 = await sut.Get(3);

                //Assert
                Assert.AreEqual(3, result.Count());

                //Assert.AreEqual(country.ID, result.ID);
                //Assert.AreEqual(country.Name, result.Name);
                //Assert.AreEqual(country1.ID, result.ID);
                //Assert.AreEqual(country1.Name, result.Name);
                //Assert.AreEqual(country2.ID, result.ID);
                //Assert.AreEqual(country2.Name, result.Name);
            }
        }
    }
}
