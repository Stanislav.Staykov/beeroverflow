﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.CountryServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
            //var options = new DbContextOptionsBuilder<DbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            //var providerMock = new Mock<IDateTimeProvider>();

            var country = new Country
            {
                ID = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);

                //Act
                var result = await sut.Delete(1);
                var actual = assertContext.Countries.FirstOrDefault(c => c.ID == 1);

                //Assert
                Assert.IsTrue(result);
                //Assert.IsTrue(actual.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new CountryService(assertContext);

                //Act
                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public async Task CorrectlySetDeleteValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));
            //var providerMock = new Mock<IDateTimeProvider>();

            //providerMock
            //    .Setup(x => x.GetDateTime())
            //    .Returns(DateTime.Parse("10, 10, 2020"));

            var country = new Country
            {
                ID = 1,
                Name = "Bulgaria"
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Countries.Add(country);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new CountryService(actContext);
                var result = await sut.Delete(1);
                Assert.IsTrue(result);
            }            
            
            using (var asertContext = new DBContext(options))
            {
                var sut = new CountryService(asertContext);
                var result = asertContext.Countries.FirstOrDefault(c => c.ID == 1);               
                Assert.IsNull(result);
            }           
        }
    }
}
