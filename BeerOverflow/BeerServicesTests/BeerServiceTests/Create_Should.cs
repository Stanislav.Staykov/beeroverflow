﻿using BeerOverflow.Database;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.BeerServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnBeer_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnBeer_When_ParamsAreValid));
           
            var beerDTO = new BeerDTO
            { 
                ID = 1,
                BreweryID = 3,
                Name = "London Pride",
                Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                StyleID = 3,
                //Review = "a",
                ABV = "4.1% (cask and keg); 4.7% (bottled)"               
            };

            using (var arrangeContext = new DBContext(options))
            {
                var sut = new BeerService(arrangeContext, new RatingService(arrangeContext));
                var newBeer = await sut.Create(beerDTO);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new BeerService(assertContext, new RatingService(assertContext));

                //Act               
                var actual = assertContext.Beers.FirstOrDefault(b => b.ID == 1);

                //Assert
                Assert.AreEqual(beerDTO.Name, actual.Name);
                Assert.IsFalse(actual.IsDeleted);
            }
        }
    }
}
