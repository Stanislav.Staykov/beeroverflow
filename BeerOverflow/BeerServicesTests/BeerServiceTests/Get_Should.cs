﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.BeerServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public async Task ReturnCorrectBeerDTO_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnCorrectBeerDTO_When_ParamsAreValid));

            var beer = new BeerOverflow.Services.DTOs.BeerDTO
            {
                ID = 1,
                BreweryID = 1,
                Name = "London Pride",
                Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                StyleID = 1,         
                ABV = "4.1% (cask and keg); 4.7% (bottled)"
            };

            var brewery = new Brewery
            {
                ID = 1,
                Name="London Brewery" ,
                CountryID=1
            }; 
            var country = new Country
            {
                ID = 1,
                Name="UK"               
            };
            var style = new Style
            {
                ID = 1,
                Name="Bark Moon"               
            };           

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Beers.Add(beer.ToModel());
                arrangeContext.Breweries.Add(brewery);
                arrangeContext.Countries.Add(country);
                arrangeContext.Styles.Add(style);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new BeerService(actContext, new RatingService(actContext));
                var result = await sut.Get(1);

                //Assert
                Assert.AreEqual(beer.ID, result.ID);
                Assert.AreEqual(beer.Name, result.Name);
                Assert.AreEqual(beer.Description, result.Description);
            }
        }

        [TestMethod]
        public void  Throw_When_BeerNotFound()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(Throw_When_BeerNotFound));
            
            //Act & Assert
            using (var actContext = new DBContext(options))
            {
                var sut = new BeerService(actContext, new RatingService(actContext));

                Assert.ThrowsExceptionAsync<ArgumentNullException>(async () => await sut.Get(1));
            }
        }
    }
}
