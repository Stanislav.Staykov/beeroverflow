﻿
using BeerOverflow.Database;
using BeerOverflow.Services;
using BeerServicesTests;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Beer.ServicesTests.BeerServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task ReturnTrue_When_ParamsAreValid()
        {
            //Arrange
             var options = Utils.GetOptions(nameof(ReturnTrue_When_ParamsAreValid));
            //var options = new DbContextOptionsBuilder<DbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString()).Options;

            //var providerMock = new Mock<IDateTimeProvider>();
        
            var beer = new BeerOverflow.Models.Beer()
            {
                ID = 1,
                BreweryID = 3,
                Name = "London Pride",
                Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                StyleID = 3,
                //Review = "a",
                ABV = "4.1% (cask and keg); 4.7% (bottled)",
                CreatedOn = DateTime.Parse("10/10/2010")
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new BeerService(assertContext, new RatingService(assertContext));

                //Act
                var result = await sut.Delete(1);
                var actual = assertContext.Beers.FirstOrDefault(b => b.ID == 1);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actual.IsDeleted);
            }
        }

        [TestMethod]
        public async Task ReturnFalse_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnFalse_When_ParamsAreValid));

            //var providerMock = new Mock<IDateTimeProvider>();

            using (var assertContext = new DBContext(options))
            {
                var sut = new BeerService(assertContext, new RatingService(assertContext));

                //Act
                var result = await sut.Delete(1);

                //Assert
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public void CorrectlySetDeleteValues_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(CorrectlySetDeleteValues_When_ParamsAreValid));
            //var providerMock = new Mock<IDateTimeProvider>();

            //providerMock
            //    .Setup(x => x.GetDateTime())
            //    .Returns(DateTime.Parse("10, 10, 2020"));

            var beer = new BeerOverflow.Models.Beer
            {
                ID = 1,
                BreweryID = 3,
                Name = "London Pride",
                Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                StyleID = 3,
                //Review = "a",
                ABV = "4.1% (cask and keg); 4.7% (bottled)",
                CreatedOn = DateTime.Parse("10/10/2010")
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Beers.Add(beer);
                arrangeContext.SaveChanges();
            }

            using (var actContext = new DBContext(options))
            {
                var sut = new BeerService(actContext, new RatingService(actContext));
                _ = sut.Delete(1);
            }

            using (var assertContext = new DBContext(options))
            {
                var actual = assertContext.Beers.First(x => x.ID == 1);

                Assert.IsTrue(actual.IsDeleted);
               // Assert.AreEqual(DateTime.Now, actual.DeletedOn);
            }
        }
    }
}
