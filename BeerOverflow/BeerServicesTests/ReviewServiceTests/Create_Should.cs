﻿using BeerOverflow.Database;
using BeerOverflow.Services;
using BeerOverflow.Services.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerServicesTests.ReviewServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task ReturnReview_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnReview_When_ParamsAreValid));

            var reviewDTO = new ReviewDTO
            {
                Body = "It is the best beer",
                UserID = 1,
                BeerID = 1
            };

            using (DBContext arrangeContext = new DBContext(options))
            {
                var sut = new ReviewService(arrangeContext);
                var newBeer = await sut.Create(reviewDTO);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new DBContext(options))
            {
                var sut = new RatingService(assertContext);

                //Act               
                var actual = assertContext.Ratings.FirstOrDefault(r => r.UserID == 1 & r.BeerID == 1);

                //Assert
                Assert.AreEqual(reviewDTO.Body, "It is the best beer");

            }
        }
    }
}
