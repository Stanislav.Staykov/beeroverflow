﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services;
using BeerOverflow.Services.Mappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerServicesTests.ReviewServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void ReturnReview_When_ParamsAreValid()
        {
            //Arrange
            var options = Utils.GetOptions(nameof(ReturnReview_When_ParamsAreValid));

            var review = new Review
            {
                Body = "The best beer",
                UserID = 1,
                BeerID = 1
            };

            var user = new User
            {
                Id = 1,
                UserName = "Batman"
            };

            using (var arrangeContext = new DBContext(options))
            {
                arrangeContext.Reviews.Add(review);
                arrangeContext.Users.Add(user);
                arrangeContext.SaveChanges();
            }

            //Act
            using (var actContext = new DBContext(options))
            {
                var sut = new ReviewService(actContext);
                var result = sut.Get(1);

                //Assert
                Assert.AreEqual(review.Body, "The best beer");
            }
        }
    }
}
