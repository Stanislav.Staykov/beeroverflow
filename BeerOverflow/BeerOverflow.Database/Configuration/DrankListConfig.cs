﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace BeerOverflow.Database.Configuration
{
    public class DrankListConfig : IEntityTypeConfiguration<DrankList>
    {
        public void Configure(EntityTypeBuilder<DrankList> builder)
        {
            builder.HasKey(e => new { e.BeerId, e.UserId });

            builder.HasOne(d => d.Beers)
                 .WithMany(b => b.DrankLists)
                 .HasForeignKey(d => d.BeerId);

            builder.HasOne(d => d.Users)
                .WithMany(u => u.DrankLists)
                .HasForeignKey(d => d.UserId);
        }
    }
}
