﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BeerOverflow.Database.Configuration
{
    public class WishListConfig : IEntityTypeConfiguration<WishList>
    {
        public void Configure(EntityTypeBuilder<WishList> builder)
        {
            builder.HasKey(e => new { e.BeerId, e.UserId });

            builder.HasOne(d => d.Beers)
                 .WithMany(b => b.WishLists)
                 .HasForeignKey(d => d.BeerId);

            builder.HasOne(d => d.Users)
                .WithMany(u => u.WishLists)
                .HasForeignKey(d => d.UserId);
        }
    }
}
