﻿using BeerOverflow.Database.Configuration;
using BeerOverflow.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace BeerOverflow.Database
{
    public class DBContext : IdentityDbContext<User, Role, int>
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Style> Styles { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<WishList> WishList { get; set; }
        public DbSet<DrankList> DrankList { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Rating> Ratings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BreweryConfig());
            modelBuilder.ApplyConfiguration(new BeerConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new WishListConfig());
            modelBuilder.ApplyConfiguration(new DrankListConfig());
            modelBuilder.ApplyConfiguration(new ReviewConfig());
            modelBuilder.ApplyConfiguration(new RatingConfig());
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Beer>().HasData
            (
                new Beer
                {
                    ID = 1,
                    BreweryID = 6,
                    Name = "London Pride",
                    Description = "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.",
                    StyleID = 8,
                    ABV = "4.1% (cask and keg); 4.7% (bottled)"
                },
                new Beer
                {
                    ID = 2,
                    BreweryID = 6,
                    Name = "Frontier",
                    Description = "Frontier is the first of its kind for Fuller’s, a craft lager that pushes the boundaries to deliver  refreshment, but not at the expense of flavour. Crafted for 42 days, using a blend of new world hops and old world brewing    techniques for a memorable tasting beer.",
                    StyleID = 4,
                    ABV = "4.5%"
                },
                new Beer
                {
                    ID = 3,
                    BreweryID = 6,
                    Name = "Honey Dew",
                    Description = "Fuller’s Organic Honey Dew is an award-winning, refreshingly golden beer bursting with subtle honey flavours.    Brewed using organic English malts and the finest organic honey, it has a sweet, mellow, rounded character, with a     delicious zesty edge from the English organic hops. Get this great organic beer delivered today!",
                    StyleID = 1,
                    ABV = "5%"
                },
                new Beer
                {
                    ID = 4,
                    BreweryID = 5,
                    Name = "Guinness Foreign Ext",
                    Description = "FuFruit and caramel flavors begin, smoky notes and a vibrant bitterness follow. Where extra hops and a stronger alcohol percentage were once used to preserve the beer, allowing it to survive and thrive during long sea voyages, now they yield its bold taste and unique flavor profile.",
                    StyleID = 7,
                    ABV = "7.5%"
                }
            );

            modelBuilder.Entity<Style>().HasData
            (
                new Style
                {
                    ID = 1,
                    Name = "Classic English-Style Pale Ale"
                },
                new Style
                {
                    ID = 2,
                    Name = "Dark American-Belgo-Style Ale"
                },
                new Style
                {
                    ID = 3,
                    Name = "Scotch Ale"
                },
                new Style
                {
                    ID = 4,
                    Name = "Lager"
                },
                new Style
                {
                    ID = 5,
                    Name = "Weiss"
                },
                new Style
                {
                    ID = 6,
                    Name = "IPA"
                },
                new Style
                {
                    ID = 7,
                    Name = "Stout"
                },
                new Style
                {
                    ID = 8,
                    Name = "Bitter - English"
                }
            );

            modelBuilder.Entity<Country>().HasData
            (
                new Country
                {
                    ID = 1,
                    Name = "Belgium"
                },
                new Country
                {
                    ID = 2,
                    Name = "United Kingdom"
                },
                new Country
                {
                    ID = 3,
                    Name = "Germany"
                },
                new Country
                {
                    ID = 4,
                    Name = "Hungary"
                },
                new Country
                {
                    ID = 5,
                    Name = "Bulgaria"
                },
                new Country
                {
                    ID = 6,
                    Name = "Republic of Ireland"
                }
            );

            modelBuilder.Entity<Brewery>().HasData
            (
                new Brewery
                {
                    ID = 1,
                    Name = "Jennings Brewery",
                    CountryID = 2
                },
                new Brewery
                {
                    ID = 2,
                    Name = "Heineken Hungaria",
                    CountryID = 4
                },
                new Brewery
                {
                    ID = 3,
                    Name = "De Proef Brouwerij",
                    CountryID = 1
                },
                new Brewery
                {
                    ID = 4,
                    Name = "Shumensko",
                    CountryID = 5
                },
                new Brewery
                {
                    ID = 5,
                    Name = "Guinness",
                    CountryID = 6
                },
                new Brewery
                {
                    ID = 6,
                    Name = "Fuller's",
                    CountryID = 2
                }
            );

            modelBuilder.Entity<Role>().HasData
            (
                new Role
                {
                    Id = 1,
                    Name = "User",
                    NormalizedName = "USER"
                },

                new Role
                {
                    Id = 2,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                }
            );

            var hasher = new PasswordHasher<User>();

            var adminUser = new User();
            var normalUser = new User();

            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.Name = "Pavel Katsarov";
            adminUser.Age = 32;
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = hasher.HashPassword(adminUser, "Admin@123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();

            normalUser.Id = 2;
            normalUser.UserName = "user@user.com";
            normalUser.Name = "Pesho Popov";
            normalUser.Age = 26;
            normalUser.NormalizedUserName = "USER@USER.COM";
            normalUser.Email = "user@user.com";
            normalUser.NormalizedEmail = "USER@USER.COM";
            normalUser.PasswordHash = hasher.HashPassword(normalUser, "User@123");
            normalUser.SecurityStamp = Guid.NewGuid().ToString();

            modelBuilder.Entity<User>().HasData
            (
                adminUser,

                normalUser
            );

            modelBuilder.Entity<IdentityUserRole<int>>().HasData
            (
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = 2
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = 1
                }
            );

            modelBuilder.Entity<DrankList>().HasData
            (
                new DrankList
                {
                    UserId = 1,
                    BeerId = 3
                },
                new DrankList
                {
                    UserId = 2,
                    BeerId = 1
                }
                //new DrankList
                //{
                //    UserId = 4,
                //    BeerId = 1
                //}, 
                //new DrankList
                //{
                //    UserId = 4,
                //    BeerId = 3
                //}
            );

            modelBuilder.Entity<WishList>().HasData
            (
                new WishList
                {
                    UserId = 1,
                    BeerId = 3
                },
                new WishList
                {
                    UserId = 1,
                    BeerId = 2
                },
                //new WishList
                //{
                //    UserId = 4,
                //    BeerId = 2
                //},
                new WishList
                {
                    UserId = 2,
                    BeerId = 3
                }
            );
        }
    }
}