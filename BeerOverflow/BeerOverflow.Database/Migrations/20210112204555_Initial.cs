﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerOverflow.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 111, nullable: false),
                    Age = table.Column<int>(nullable: false),
                    BanDescription = table.Column<string>(nullable: true),
                    BannedOn = table.Column<DateTime>(nullable: false),
                    UnbannedOn = table.Column<DateTime>(nullable: false),
                    ReviewID = table.Column<int>(nullable: true),
                    RatingID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 111, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Styles",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 111, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Styles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Breweries",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 111, nullable: false),
                    CountryID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Breweries", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Breweries_Countries_CountryID",
                        column: x => x.CountryID,
                        principalTable: "Countries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Beers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 23, nullable: false),
                    Description = table.Column<string>(maxLength: 551, nullable: false),
                    ABV = table.Column<string>(nullable: false),
                    BreweryID = table.Column<int>(nullable: true),
                    StyleID = table.Column<int>(nullable: true),
                    Rating = table.Column<int>(nullable: false),
                    ReviewID = table.Column<int>(nullable: true),
                    RatingID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Beers_Breweries_BreweryID",
                        column: x => x.BreweryID,
                        principalTable: "Breweries",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_Styles_StyleID",
                        column: x => x.StyleID,
                        principalTable: "Styles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DrankList",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    BeerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrankList", x => new { x.BeerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_DrankList_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrankList_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ratings",
                columns: table => new
                {
                    BeerID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ratings", x => new { x.BeerID, x.UserID });
                    table.ForeignKey(
                        name: "FK_Ratings_Beers_BeerID",
                        column: x => x.BeerID,
                        principalTable: "Beers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ratings_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    BeerID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ModifiedOn = table.Column<DateTime>(nullable: true),
                    DeletedOn = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    Body = table.Column<string>(maxLength: 551, nullable: false),
                    Likes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => new { x.BeerID, x.UserID });
                    table.ForeignKey(
                        name: "FK_Reviews_Beers_BeerID",
                        column: x => x.BeerID,
                        principalTable: "Beers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reviews_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WishList",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    BeerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WishList", x => new { x.BeerId, x.UserId });
                    table.ForeignKey(
                        name: "FK_WishList_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WishList_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "271b7eba-a5bb-4a09-a49b-d7400288cc79", "Admin", "ADMIN" },
                    { 1, "1f61177e-b3f3-4099-9c44-9943ee2a8bc1", "User", "USER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "Age", "BanDescription", "BannedOn", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "Name", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "RatingID", "ReviewID", "SecurityStamp", "TwoFactorEnabled", "UnbannedOn", "UserName" },
                values: new object[,]
                {
                    { 2, 0, 26, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "5f623fbe-cf97-478d-990c-70354fcb6550", "user@user.com", false, false, null, "Pesho Popov", "USER@USER.COM", "USER@USER.COM", "AQAAAAEAACcQAAAAEK+ypjiY4CeUHSFw6sWLFFLetoodOGsGQQ/V8+f9MO/sip/BIdgOT/dbNx4RABX8oQ==", null, false, null, null, "19298324-9f27-469a-b88d-bd5c765de84d", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "user@user.com" },
                    { 1, 0, 32, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "cd84b04d-e879-4f28-988b-3137d6fb63c9", "admin@admin.com", false, false, null, "Pavel Katsarov", "ADMIN@ADMIN.COM", "ADMIN@ADMIN.COM", "AQAAAAEAACcQAAAAEHp0HtaYPRqa/Cow0mABB8uz7ScmWNeM6wzF865MWu9MHmtIhLfsHtuXH+4XUzJV1g==", null, false, null, null, "8be90b30-958a-4185-935d-d7ba2b19765c", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "admin@admin.com" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 6, "Republic of Ireland" },
                    { 1, "Belgium" },
                    { 4, "Hungary" },
                    { 3, "Germany" },
                    { 2, "United Kingdom" },
                    { 5, "Bulgaria" }
                });

            migrationBuilder.InsertData(
                table: "Styles",
                columns: new[] { "ID", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 2, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2216), null, false, null, "Dark American-Belgo-Style Ale" },
                    { 3, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2249), null, false, null, "Scotch Ale" },
                    { 4, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2253), null, false, null, "Lager" },
                    { 5, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2256), null, false, null, "Weiss" },
                    { 6, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2258), null, false, null, "IPA" },
                    { 7, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2261), null, false, null, "Stout" },
                    { 8, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(2264), null, false, null, "Bitter - English" },
                    { 1, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(406), null, false, null, "Classic English-Style Pale Ale" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 2, 1 }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "ID", "CountryID", "CreatedOn", "DeletedOn", "IsDeleted", "ModifiedOn", "Name" },
                values: new object[,]
                {
                    { 3, 1, new DateTime(2021, 1, 12, 20, 45, 54, 432, DateTimeKind.Utc).AddTicks(507), null, false, null, "De Proef Brouwerij" },
                    { 1, 2, new DateTime(2021, 1, 12, 20, 45, 54, 431, DateTimeKind.Utc).AddTicks(7896), null, false, null, "Jennings Brewery" },
                    { 6, 2, new DateTime(2021, 1, 12, 20, 45, 54, 432, DateTimeKind.Utc).AddTicks(516), null, false, null, "Fuller's" },
                    { 2, 4, new DateTime(2021, 1, 12, 20, 45, 54, 432, DateTimeKind.Utc).AddTicks(462), null, false, null, "Heineken Hungaria" },
                    { 4, 5, new DateTime(2021, 1, 12, 20, 45, 54, 432, DateTimeKind.Utc).AddTicks(510), null, false, null, "Shumensko" },
                    { 5, 6, new DateTime(2021, 1, 12, 20, 45, 54, 432, DateTimeKind.Utc).AddTicks(513), null, false, null, "Guinness" }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "ID", "ABV", "BreweryID", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "ModifiedOn", "Name", "Rating", "RatingID", "ReviewID", "StyleID" },
                values: new object[,]
                {
                    { 1, "4.1% (cask and keg); 4.7% (bottled)", 6, new DateTime(2021, 1, 12, 20, 45, 54, 428, DateTimeKind.Utc).AddTicks(3585), null, "It’s a beautiful thing to be able to pour your own pint of Pride - and this 5 litre mini-keg, keeps the beers    flowing freely. It holds no fewer than 8 pints of our revered London Pride and represents the ultimate way to enjoy the    nation’s favourite ale at home.", false, null, "London Pride", 0, null, null, 8 },
                    { 2, "4.5%", 6, new DateTime(2021, 1, 12, 20, 45, 54, 428, DateTimeKind.Utc).AddTicks(8914), null, "Frontier is the first of its kind for Fuller’s, a craft lager that pushes the boundaries to deliver  refreshment, but not at the expense of flavour. Crafted for 42 days, using a blend of new world hops and old world brewing    techniques for a memorable tasting beer.", false, null, "Frontier", 0, null, null, 4 },
                    { 3, "5%", 6, new DateTime(2021, 1, 12, 20, 45, 54, 428, DateTimeKind.Utc).AddTicks(8980), null, "Fuller’s Organic Honey Dew is an award-winning, refreshingly golden beer bursting with subtle honey flavours.    Brewed using organic English malts and the finest organic honey, it has a sweet, mellow, rounded character, with a     delicious zesty edge from the English organic hops. Get this great organic beer delivered today!", false, null, "Honey Dew", 0, null, null, 1 },
                    { 4, "7.5%", 5, new DateTime(2021, 1, 12, 20, 45, 54, 428, DateTimeKind.Utc).AddTicks(8986), null, "FuFruit and caramel flavors begin, smoky notes and a vibrant bitterness follow. Where extra hops and a stronger alcohol percentage were once used to preserve the beer, allowing it to survive and thrive during long sea voyages, now they yield its bold taste and unique flavor profile.", false, null, "Guinness Foreign Ext", 0, null, null, 7 }
                });

            migrationBuilder.InsertData(
                table: "DrankList",
                columns: new[] { "BeerId", "UserId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 3, 1 }
                });

            migrationBuilder.InsertData(
                table: "WishList",
                columns: new[] { "BeerId", "UserId" },
                values: new object[,]
                {
                    { 2, 1 },
                    { 3, 1 },
                    { 3, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_BreweryID",
                table: "Beers",
                column: "BreweryID");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_StyleID",
                table: "Beers",
                column: "StyleID");

            migrationBuilder.CreateIndex(
                name: "IX_Breweries_CountryID",
                table: "Breweries",
                column: "CountryID");

            migrationBuilder.CreateIndex(
                name: "IX_DrankList_UserId",
                table: "DrankList",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_UserID",
                table: "Ratings",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_UserID",
                table: "Reviews",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_WishList_UserId",
                table: "WishList",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DrankList");

            migrationBuilder.DropTable(
                name: "Ratings");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "WishList");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Beers");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Breweries");

            migrationBuilder.DropTable(
                name: "Styles");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
