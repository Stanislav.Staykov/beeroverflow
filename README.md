# BeerOverflow
### Database Diagram

![](./img/dbdiagram.png)


# BeerOverflow

BeerOverflow is a website that combines all your favorite beers in one place allowing you to keep track of beers you are interested in as well as beers you've already drank.

# Features

  - Add beer to wishlist
  - Add beer to dranklist
  - Review beer
  - Rate beer

You can also:
  - Create beers
  - Add countries
  - Add breweries
  - Add styles
  - Edit all of the above


# API

You can get all of the site's data via API calls:

```sh
https://domain/api/Beers
https://domain/api/Countries
https://domain/api/Styles
https://domain/api/Breweries
```

Get single item by specifying ID:

```sh
https://domain/api/Beers/id
https://domain/api/Countries/id
https://domain/api/Styles/id
https://domain/api/Breweries/id
```

Get WishList and DrankList by specifying userId

```sh
https://domain/api/DrankList/userId
https://domain/api/WishList/userId
```

Get Ratings and Reviews by specifying beerId

```sh
https://domain/api/Ratings/beer/beerId
https://domain/api/Reviews/beer/beerId
```

### Filters

The api provides filtering and sorting functionality. Use as per the example bellow:

| Operation | Query |
| ------ | ------ |
| Filter | ?filterBy=country&filterValue=Belgium |
| Sort | ?sortBy=name&order=desc |
| Search | ?search=guinness |
| Page | ?pageSize=20&page=1 |

# Website

![](./img/navbar.jpg)


The navbar shows you the site's navigation. It will display additional elements based on wether or not a use is signed in and that user's role.

![](./img/wishdranklist.jpg)


The register and login forms allow you to register and login.


![](./img/register.jpg)
![](./img/login.jpg)


After logging in, a user can add beers to their wishlist or dranklist using either the buttons in the Beers section


![](./img/beercontrols.jpg)


or the Details page.


![](./img/beerdetails.jpg)


While on this page a user can rate and review a beer as well as see other ratings and reviews. Your reviews are labeled 'You'.


![](./img/ratereview.jpg)


The main Beers page can be filtered 


![](./img/beerfilters.jpg)


and sorted.


![](./img/beersort.jpg)


Administrators can display a Users page where they can perform various operations.


![](./img/usercontrols.jpg)

